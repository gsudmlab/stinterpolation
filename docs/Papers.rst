Papers
==========

If you are using STInterpolation, please cite the following papers accordingly:

Research Papers:
---------------------

- Boubrahimi, Soukaina Filali, et al. "Filling the gaps in solar big data: Interpolation of solar filament event instances." Big Data and Cloud Computing (BDCloud), Social Computing and Networking (SocialCom), Sustainable Computing and Communications (SustainCom)(BDCloud-SocialCom-SustainCom), 2016 IEEE International Conferences on. IEEE, 2016.
- Boubrahimi, Soukaina Filali, et al. "Spatio-temporal interpolation methods for solar events metadata." Big Data (Big Data), 2016 IEEE International Conference on. IEEE, 2016.
- Boubrahimi, Soukaina Filali, et al. "SOLEV: a video generation framework for solar events from mixed data sources (demo paper)." Proceedings of the 24th ACM SIGSPATIAL International Conference on Advances in Geographic Information Systems. ACM, 2016.

