.. java:import:: java.util ArrayList

.. java:import:: java.util TreeSet

.. java:import:: com.vividsolutions.jts.densify Densifier

.. java:import:: com.vividsolutions.jts.geom Geometry

.. java:import:: com.vividsolutions.jts.geom Polygon

.. java:import:: edu.gsu.dmlab.solgrind.base EventType

.. java:import:: edu.gsu.dmlab.solgrind.base Instance

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential TGPair

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential TInterval

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential Trajectory

.. java:import:: edu.gsu.dmlab.util InterpolationConstants

.. java:import:: edu.gsu.dmlab.util InterpolationConstants.RoundingStrategy

Interpolator
============

.. java:package:: edu.gsu.dmlab.edu.gsu.dmlab.interpolation
   :noindex:

.. java:type:: public abstract class Interpolator

Methods
-------
adjustTimestamps
^^^^^^^^^^^^^^^^

.. java:method:: public static Instance adjustTimestamps(Instance ins)
   :outertype: Interpolator

densify
^^^^^^^

.. java:method:: public static Geometry densify(Polygon geom)
   :outertype: Interpolator

interpolate
^^^^^^^^^^^

.. java:method:: public abstract Instance interpolate(Instance ins)
   :outertype: Interpolator

interpolateAtTime
^^^^^^^^^^^^^^^^^

.. java:method:: public abstract TGPair interpolateAtTime(TGPair tgPair, TGPair tgPair2, long i)
   :outertype: Interpolator

interpolateTGPair
^^^^^^^^^^^^^^^^^

.. java:method:: public abstract TreeSet<TGPair> interpolateTGPair(TGPair tgp, TGPair nextTgp, EventType et)
   :outertype: Interpolator

isValidEventType
^^^^^^^^^^^^^^^^

.. java:method:: public abstract boolean isValidEventType(EventType e)
   :outertype: Interpolator

selectInterpolator
^^^^^^^^^^^^^^^^^^

.. java:method:: public static Interpolator selectInterpolator(EventType e)
   :outertype: Interpolator

