.. java:import:: java.util ArrayList

.. java:import:: java.util TreeSet

.. java:import:: com.vividsolutions.jts.geom Envelope

.. java:import:: com.vividsolutions.jts.geom Geometry

.. java:import:: com.vividsolutions.jts.geom GeometryFactory

.. java:import:: com.vividsolutions.jts.geom Polygon

.. java:import:: edu.gsu.dmlab.solgrind.base EventType

.. java:import:: edu.gsu.dmlab.solgrind.base Instance

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential TGPair

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential TInterval

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential Trajectory

.. java:import:: edu.gsu.dmlab.util InterpolationConstants

.. java:import:: edu.gsu.dmlab.util PosisitonEstimator

MBRInterpolator
===============

.. java:package:: edu.gsu.dmlab.edu.gsu.dmlab.interpolation
   :noindex:

.. java:type:: public class MBRInterpolator extends Interpolator

Fields
------
gf
^^

.. java:field::  GeometryFactory gf
   :outertype: MBRInterpolator

Methods
-------
interpolate
^^^^^^^^^^^

.. java:method:: @Override public Instance interpolate(Instance ins)
   :outertype: MBRInterpolator

interpolateAtTime
^^^^^^^^^^^^^^^^^

.. java:method:: @Override public TGPair interpolateAtTime(TGPair tgp, TGPair nextTgp, long time)
   :outertype: MBRInterpolator

interpolateTGPair
^^^^^^^^^^^^^^^^^

.. java:method:: @Override public TreeSet<TGPair> interpolateTGPair(TGPair tgp, TGPair nextTgp, EventType et)
   :outertype: MBRInterpolator

isValidEventType
^^^^^^^^^^^^^^^^

.. java:method:: @Override public boolean isValidEventType(EventType e)
   :outertype: MBRInterpolator

