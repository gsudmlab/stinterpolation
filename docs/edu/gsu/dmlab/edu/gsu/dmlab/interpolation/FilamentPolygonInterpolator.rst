.. java:import:: java.util ArrayList

.. java:import:: java.util Collections

.. java:import:: java.util List

.. java:import:: java.util Map.Entry

.. java:import:: java.util Set

.. java:import:: java.util TreeMap

.. java:import:: java.util TreeSet

.. java:import:: org.apache.commons.math3.util Pair

.. java:import:: com.vividsolutions.jts.geom Coordinate

.. java:import:: com.vividsolutions.jts.geom Geometry

.. java:import:: com.vividsolutions.jts.geom Polygon

.. java:import:: edu.gsu.dmlab.solgrind.base EventType

.. java:import:: edu.gsu.dmlab.solgrind.base Instance

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential TGPair

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential Trajectory

.. java:import:: edu.gsu.dmlab.util Cluster

.. java:import:: edu.gsu.dmlab.util InterpolationConstants

.. java:import:: edu.gsu.dmlab.util KMeans

.. java:import:: edu.gsu.dmlab.util PosisitonEstimator

.. java:import:: net.sf.javaml.distance.fastdtw.dtw TimeWarpInfo

.. java:import:: net.sf.javaml.distance.fastdtw.timeseries TimeSeries

FilamentPolygonInterpolator
===========================

.. java:package:: edu.gsu.dmlab.edu.gsu.dmlab.interpolation
   :noindex:

.. java:type:: public class FilamentPolygonInterpolator extends ComplexPolygonInterpolator

Methods
-------
findEndpointsfromClusters
^^^^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public static Pair<Coordinate, Coordinate> findEndpointsfromClusters(List<Cluster> clusters)
   :outertype: FilamentPolygonInterpolator

   Finds the most distant pairs of coordinates belonging to 2 different clusters

   :param clusters:
   :return: the two endpoints of a polygon

getKMeansCentroids
^^^^^^^^^^^^^^^^^^

.. java:method:: public static Pair<Coordinate, Coordinate> getKMeansCentroids(List<Coordinate> mostDistantCoordinates)
   :outertype: FilamentPolygonInterpolator

   Returns the centroids of the clusters of the 2-Means algorithm.

   :param mostDistantCoordinates:

getMostDistantCoordinates
^^^^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public static List<Coordinate> getMostDistantCoordinates(double PERCENTAGE_COORDINATE, Polygon polygon)
   :outertype: FilamentPolygonInterpolator

   Finds a percentage of the most distant coordinate pairs of a polygon

   :param PERCENTAGE_COORDINATE:
   :param polygon:
   :return: a percentage of most distant coordinate pairs

interpolate
^^^^^^^^^^^

.. java:method:: @Override public Instance interpolate(Instance ins)
   :outertype: FilamentPolygonInterpolator

interpolateAtTime
^^^^^^^^^^^^^^^^^

.. java:method:: @Override public TGPair interpolateAtTime(TGPair tgp, TGPair nextTgp, long time)
   :outertype: FilamentPolygonInterpolator

   Uses the 5-step filament interpolation to interpolated a segment trajectory of 2 tg-pairs.

   :param tgp: The first tg-pair to be used in the interpolation
   :param nextTgp: The second tg-pair to be used in the interpolation
   :param time: The time to be interpolated in

interpolateTGPair
^^^^^^^^^^^^^^^^^

.. java:method:: @Override public TreeSet<TGPair> interpolateTGPair(TGPair tgp, TGPair nextTgp, EventType et)
   :outertype: FilamentPolygonInterpolator

isValidEventType
^^^^^^^^^^^^^^^^

.. java:method:: @Override public boolean isValidEventType(EventType e)
   :outertype: FilamentPolygonInterpolator

rearrangePolygon
^^^^^^^^^^^^^^^^

.. java:method:: static Geometry rearrangePolygon(Polygon polygon, Coordinate endpoint)
   :outertype: FilamentPolygonInterpolator

   Reorganize a polygon such that it starts(and ends) with the input Coordinate.

   :param polygon:
   :param endpoint:
   :return: the new re-arragend geometry that starts from the best endpoint

