SolgrindConstants
=================

.. java:package:: edu.gsu.dmlab.solgrind
   :noindex:

.. java:type:: public class SolgrindConstants

Fields
------
BD
^^

.. java:field:: public static final double BD
   :outertype: SolgrindConstants

CCE_th
^^^^^^

.. java:field:: public static final double CCE_th
   :outertype: SolgrindConstants

CI_th
^^^^^

.. java:field:: public static final double CI_th
   :outertype: SolgrindConstants

DATASET_DIR
^^^^^^^^^^^

.. java:field:: public static final String DATASET_DIR
   :outertype: SolgrindConstants

H_R
^^^

.. java:field:: public static final double H_R
   :outertype: SolgrindConstants

H_in
^^^^

.. java:field:: public static final long H_in
   :outertype: SolgrindConstants

SAMPLING_INTERVAL
^^^^^^^^^^^^^^^^^

.. java:field:: public static final long SAMPLING_INTERVAL
   :outertype: SolgrindConstants

TV
^^

.. java:field:: public static final long TV
   :outertype: SolgrindConstants

T_R
^^^

.. java:field:: public static final double T_R
   :outertype: SolgrindConstants

T_in
^^^^

.. java:field:: public static final long T_in
   :outertype: SolgrindConstants

