.. java:import:: edu.gsu.dmlab.solgrind.base EventType

.. java:import:: java.util ArrayList

.. java:import:: java.util.stream Collectors

EventSequence
=============

.. java:package:: edu.gsu.dmlab.solgrind.base.types.event
   :noindex:

.. java:type:: public class EventSequence implements Comparable<EventSequence>

Constructors
------------
EventSequence
^^^^^^^^^^^^^

.. java:constructor:: public EventSequence()
   :outertype: EventSequence

EventSequence
^^^^^^^^^^^^^

.. java:constructor:: public EventSequence(EventType e)
   :outertype: EventSequence

EventSequence
^^^^^^^^^^^^^

.. java:constructor:: public EventSequence(ArrayList<EventType> eventTypes)
   :outertype: EventSequence

Methods
-------
appendEventType
^^^^^^^^^^^^^^^

.. java:method:: public EventSequence appendEventType(EventType type)
   :outertype: EventSequence

compareTo
^^^^^^^^^

.. java:method:: @Override public int compareTo(EventSequence o)
   :outertype: EventSequence

connect
^^^^^^^

.. java:method:: public static EventSequence connect(EventSequence fs1, EventSequence fs2)
   :outertype: EventSequence

   Always check whether those two matches or not!!!

   :param fs1:
   :param fs2:
   :return: connected sequence from fs1 and fs2

equals
^^^^^^

.. java:method:: @Override public boolean equals(Object obj)
   :outertype: EventSequence

getEventsList
^^^^^^^^^^^^^

.. java:method:: public ArrayList<EventType> getEventsList()
   :outertype: EventSequence

getFolloweeSubsequence
^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public EventSequence getFolloweeSubsequence()
   :outertype: EventSequence

getFollowerSubsequence
^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public EventSequence getFollowerSubsequence()
   :outertype: EventSequence

getLength
^^^^^^^^^

.. java:method:: public int getLength()
   :outertype: EventSequence

getSequenceHead
^^^^^^^^^^^^^^^

.. java:method:: public EventType getSequenceHead()
   :outertype: EventSequence

getSequenceTail
^^^^^^^^^^^^^^^

.. java:method:: public EventType getSequenceTail()
   :outertype: EventSequence

getTableName
^^^^^^^^^^^^

.. java:method:: public String getTableName()
   :outertype: EventSequence

getType
^^^^^^^

.. java:method:: public EventType getType(int index)
   :outertype: EventSequence

getTypeName
^^^^^^^^^^^

.. java:method:: public String getTypeName(int index)
   :outertype: EventSequence

hashCode
^^^^^^^^

.. java:method:: @Override public int hashCode()
   :outertype: EventSequence

insert
^^^^^^

.. java:method:: public void insert(EventType type)
   :outertype: EventSequence

isValid
^^^^^^^

.. java:method:: public boolean isValid()
   :outertype: EventSequence

matches
^^^^^^^

.. java:method:: public static boolean matches(EventSequence s1, EventSequence s2)
   :outertype: EventSequence

toString
^^^^^^^^

.. java:method:: @Override public String toString()
   :outertype: EventSequence

