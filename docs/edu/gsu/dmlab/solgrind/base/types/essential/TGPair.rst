.. java:import:: com.vividsolutions.jts.geom Envelope

.. java:import:: com.vividsolutions.jts.geom Geometry

.. java:import:: com.vividsolutions.jts.geom GeometryFactory

TGPair
======

.. java:package:: edu.gsu.dmlab.solgrind.base.types.essential
   :noindex:

.. java:type:: public class TGPair implements Comparable<TGPair>

   :author: berkay - Jul 12, 2016

Constructors
------------
TGPair
^^^^^^

.. java:constructor:: public TGPair(long start, long end, Geometry geom)
   :outertype: TGPair

TGPair
^^^^^^

.. java:constructor:: public TGPair(long start, long end, Geometry geom, String KBarchivID, Boolean interpolated)
   :outertype: TGPair

TGPair
^^^^^^

.. java:constructor:: public TGPair(long start, long end, Geometry geom, String KBarchivID)
   :outertype: TGPair

TGPair
^^^^^^

.. java:constructor:: public TGPair(long stime, Geometry geom)
   :outertype: TGPair

TGPair
^^^^^^

.. java:constructor:: public TGPair(String start, String end, Geometry geom)
   :outertype: TGPair

TGPair
^^^^^^

.. java:constructor:: public TGPair(String startTimestamp, Geometry geom)
   :outertype: TGPair

Methods
-------
compareTo
^^^^^^^^^

.. java:method:: @Override public int compareTo(TGPair tgp)
   :outertype: TGPair

getEnvelope
^^^^^^^^^^^

.. java:method:: public Envelope getEnvelope()
   :outertype: TGPair

getGeometry
^^^^^^^^^^^

.. java:method:: public Geometry getGeometry()
   :outertype: TGPair

getInterpolated
^^^^^^^^^^^^^^^

.. java:method:: public boolean getInterpolated()
   :outertype: TGPair

getKBarchivID
^^^^^^^^^^^^^

.. java:method:: public String getKBarchivID()
   :outertype: TGPair

getTInterval
^^^^^^^^^^^^

.. java:method:: public TInterval getTInterval()
   :outertype: TGPair

getVolume
^^^^^^^^^

.. java:method:: public double getVolume()
   :outertype: TGPair

isValid
^^^^^^^

.. java:method:: public boolean isValid()
   :outertype: TGPair

sOverlaps
^^^^^^^^^

.. java:method:: public boolean sOverlaps(TGPair tgp)
   :outertype: TGPair

   Spatial-only overlaps method. Checks if the geometries of the objects overlap in space

   :param tgp:

setGeometry
^^^^^^^^^^^

.. java:method:: public void setGeometry(Geometry geom)
   :outertype: TGPair

setInterpolated
^^^^^^^^^^^^^^^

.. java:method:: public void setInterpolated(boolean interpolated)
   :outertype: TGPair

setKBarchivID
^^^^^^^^^^^^^

.. java:method:: public void setKBarchivID(String kBarchivID)
   :outertype: TGPair

stOverlaps
^^^^^^^^^^

.. java:method:: public boolean stOverlaps(TGPair tgp)
   :outertype: TGPair

   Spatiotemporal overlaps method. Checks if the given time geometry pair both temporally and spatially intersects with the parameter tgp.

   :return: True if overlap occurs, False otherwise

tOverlaps
^^^^^^^^^

.. java:method:: public boolean tOverlaps(TGPair tgp)
   :outertype: TGPair

   Temporal-only overlaps method Checks of the intervals of the objects overlap in time

   :param tgp:

tStartsAfter
^^^^^^^^^^^^

.. java:method:: public boolean tStartsAfter(TGPair tgp)
   :outertype: TGPair

toString
^^^^^^^^

.. java:method:: @Override public String toString()
   :outertype: TGPair

