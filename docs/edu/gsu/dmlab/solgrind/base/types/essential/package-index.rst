solgrind.base.types.essential
===========================================

.. java:package:: edu.gsu.dmlab.solgrind.base.types.essential

.. toctree::
   :maxdepth: 1

   SamplingPeriodException
   TGPair
   TInterval
   Trajectory

