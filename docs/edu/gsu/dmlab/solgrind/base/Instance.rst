.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential TInterval

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential Trajectory

Instance
========

.. java:package:: edu.gsu.dmlab.solgrind.base
   :noindex:

.. java:type:: public class Instance implements Comparable<Instance>

   Class for modeling spatiotemporal event instances. Instances are moving objects modeled by Trajectory objects

   :author: berkay - Jul 14, 2016

Constructors
------------
Instance
^^^^^^^^

.. java:constructor:: public Instance(String iid, EventType e)
   :outertype: Instance

Instance
^^^^^^^^

.. java:constructor:: public Instance(String iid)
   :outertype: Instance

Instance
^^^^^^^^

.. java:constructor:: public Instance(String iid, EventType e, Trajectory traj)
   :outertype: Instance

Methods
-------
compareTo
^^^^^^^^^

.. java:method:: public int compareTo(Instance o)
   :outertype: Instance

equals
^^^^^^

.. java:method:: public boolean equals(Instance ins)
   :outertype: Instance

getEndTime
^^^^^^^^^^

.. java:method:: public long getEndTime()
   :outertype: Instance

getId
^^^^^

.. java:method:: public String getId()
   :outertype: Instance

getInterval
^^^^^^^^^^^

.. java:method:: public TInterval getInterval()
   :outertype: Instance

getStartTime
^^^^^^^^^^^^

.. java:method:: public long getStartTime()
   :outertype: Instance

getTrajectory
^^^^^^^^^^^^^

.. java:method:: public Trajectory getTrajectory()
   :outertype: Instance

getType
^^^^^^^

.. java:method:: public EventType getType()
   :outertype: Instance

hashCode
^^^^^^^^

.. java:method:: public int hashCode()
   :outertype: Instance

setId
^^^^^

.. java:method:: public void setId(String id)
   :outertype: Instance

setType
^^^^^^^

.. java:method:: public void setType(EventType type)
   :outertype: Instance

toString
^^^^^^^^

.. java:method:: @Override public String toString()
   :outertype: Instance

