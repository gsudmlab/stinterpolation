EventType
=========

.. java:package:: edu.gsu.dmlab.solgrind.base
   :noindex:

.. java:type:: public class EventType implements Comparable<EventType>

   :author: berkay - Jul 12, 2016

Constructors
------------
EventType
^^^^^^^^^

.. java:constructor:: public EventType(String EventType)
   :outertype: EventType

Methods
-------
clone
^^^^^

.. java:method:: @Override protected Object clone() throws CloneNotSupportedException
   :outertype: EventType

compareTo
^^^^^^^^^

.. java:method:: public int compareTo(EventType o)
   :outertype: EventType

equals
^^^^^^

.. java:method:: @Override public boolean equals(Object t)
   :outertype: EventType

equalsIgnoreCase
^^^^^^^^^^^^^^^^

.. java:method:: public boolean equalsIgnoreCase(EventType t)
   :outertype: EventType

getColor
^^^^^^^^

.. java:method:: public String getColor()
   :outertype: EventType

getType
^^^^^^^

.. java:method:: public String getType()
   :outertype: EventType

hashCode
^^^^^^^^

.. java:method:: @Override public int hashCode()
   :outertype: EventType

toString
^^^^^^^^

.. java:method:: public String toString()
   :outertype: EventType

