.. java:import:: java.util Iterator

.. java:import:: java.util Set

.. java:import:: java.util TreeSet

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential TInterval

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential Trajectory

TOperations
===========

.. java:package:: edu.gsu.dmlab.solgrind.base.operations
   :noindex:

.. java:type:: public class TOperations

   Implementation of some temporal operations for Trajectory objects

   :author: berkay - Aug 1, 2016

Methods
-------
tIntersection
^^^^^^^^^^^^^

.. java:method:: public static TreeSet<TInterval> tIntersection(Trajectory traj1, Trajectory traj2)
   :outertype: TOperations

   Method for temporal intersection between two trajectory. Returns the set of time interval objects where there exists a temporal co-existence relation between two trajectories given as parameters

   :param traj1:
   :param traj2:
   :return: SortedSet of TInterval objects

tIntersects
^^^^^^^^^^^

.. java:method:: public static boolean tIntersects(Trajectory traj1, Trajectory traj2)
   :outertype: TOperations

