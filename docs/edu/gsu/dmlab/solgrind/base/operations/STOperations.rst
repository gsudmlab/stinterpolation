.. java:import:: java.util Collection

.. java:import:: java.util TreeSet

.. java:import:: com.vividsolutions.jts.geom Geometry

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential TGPair

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential TInterval

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential Trajectory

STOperations
============

.. java:package:: edu.gsu.dmlab.solgrind.base.operations
   :noindex:

.. java:type:: public class STOperations

   Implements spatiotemporal operations

   :author: berkay - Aug 1, 2016

Methods
-------
interpolate
^^^^^^^^^^^

.. java:method:: public static Geometry interpolate(Geometry geometry, Geometry nextGeometry, double samplingRatio)
   :outertype: STOperations

interpolate
^^^^^^^^^^^

.. java:method:: public static Geometry interpolate(Geometry geometry, long l)
   :outertype: STOperations

intersection
^^^^^^^^^^^^

.. java:method:: public static Trajectory intersection(Trajectory traj1, Trajectory traj2)
   :outertype: STOperations

intersectionAll
^^^^^^^^^^^^^^^

.. java:method:: public static Trajectory intersectionAll(Collection<Trajectory> trajCollection)
   :outertype: STOperations

stIntersects
^^^^^^^^^^^^

.. java:method:: public static boolean stIntersects(Trajectory traj1, Trajectory traj2)
   :outertype: STOperations

union
^^^^^

.. java:method:: public static Trajectory union(Trajectory traj1, Trajectory traj2)
   :outertype: STOperations

unionAll
^^^^^^^^

.. java:method:: public static Trajectory unionAll(Collection<Trajectory> trajCollection)
   :outertype: STOperations

