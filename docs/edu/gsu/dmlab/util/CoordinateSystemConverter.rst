.. java:import:: edu.gsu.dmlab.util Point2D

.. java:import:: org.apache.commons.math3.complex Complex

CoordinateSystemConverter
=========================

.. java:package:: edu.gsu.dmlab.util
   :noindex:

.. java:type:: public class CoordinateSystemConverter

   Used for conversion between the various coordinate systems used in solar physics datasets based on the SDO mission with its 4096 X 4096 pixel images and a pixel based coordinate system used in displaying images in computer science. implemented by the Data Mining Lab at Georgia State University Some code was taken from SunPy a python library and can be found at http://sunpy.org/

   :author: Dustin Kempton, Data Mining Lab, Georgia State University

Fields
------
CDELT
^^^^^

.. java:field:: static final double CDELT
   :outertype: CoordinateSystemConverter

HPCCENTER
^^^^^^^^^

.. java:field:: static final double HPCCENTER
   :outertype: CoordinateSystemConverter

dsun_meters
^^^^^^^^^^^

.. java:field:: static final double dsun_meters
   :outertype: CoordinateSystemConverter

rsun_meters
^^^^^^^^^^^

.. java:field:: static final double rsun_meters
   :outertype: CoordinateSystemConverter

Methods
-------
convertHCC_HG
^^^^^^^^^^^^^

.. java:method:: static Point2D convertHCC_HG(double x, double y)
   :outertype: CoordinateSystemConverter

convertHCC_HPC
^^^^^^^^^^^^^^

.. java:method:: static Point2D convertHCC_HPC(double x, double y)
   :outertype: CoordinateSystemConverter

convertHGSToPixXY
^^^^^^^^^^^^^^^^^

.. java:method:: public static Point2D convertHGSToPixXY(Point2D pointIn)
   :outertype: CoordinateSystemConverter

   convertToPixXY :converts a Point2D in HGS coordinates to Pixel XY coordinates

   :param pointIn: :Point2D to convert
   :return: :a new Point2D in Pixel XY coordinates

convertHG_HCC
^^^^^^^^^^^^^

.. java:method:: static Point2D convertHG_HCC(double hglon_deg, double hglat_deg)
   :outertype: CoordinateSystemConverter

convertHPCToPixXY
^^^^^^^^^^^^^^^^^

.. java:method:: public static Point2D convertHPCToPixXY(Point2D pointIn)
   :outertype: CoordinateSystemConverter

   convertToPixXY :converts a Point2D in HPC coordinates to Pixel XY coordinates

   :param pointIn: :Point2D to convert
   :return: :a new Point2D in Pixel XY coordinates

convertHPCToPixXY
^^^^^^^^^^^^^^^^^

.. java:method:: public static Point2D convertHPCToPixXY(Point2D pointIn, double X0, double Y0)
   :outertype: CoordinateSystemConverter

   convertToPixXY :converts a Point2D in HPC coordinates to Pixel XY coordinates

   :param pointIn: :Point2D to convert
   :return: :a new Point2D in Pixel XY coordinates

convertHPC_HCC
^^^^^^^^^^^^^^

.. java:method:: static Point2D convertHPC_HCC(double x, double y)
   :outertype: CoordinateSystemConverter

convertPixXYToHGS
^^^^^^^^^^^^^^^^^

.. java:method:: public static Point2D convertPixXYToHGS(Point2D pointIn)
   :outertype: CoordinateSystemConverter

   convertToHGS :converts a Point2D in Pixel XY coordinates to HGS coordinates

   :param pointIn: :the Point2D to convert
   :return: :a new Point2D in HGS

convertPixXYToHPC
^^^^^^^^^^^^^^^^^

.. java:method:: public static Point2D convertPixXYToHPC(Point2D pointIn)
   :outertype: CoordinateSystemConverter

   convertToHGS :converts a Point2D in Pixel XY coordinates to HPC coordinates

   :param pointIn: :the Point2D to convert
   :return: :a new Point2D in HPC

