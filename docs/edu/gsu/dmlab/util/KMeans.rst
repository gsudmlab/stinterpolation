.. java:import:: java.util ArrayList

.. java:import:: java.util List

.. java:import:: org.apache.commons.math3.util Pair

.. java:import:: com.vividsolutions.jts.geom Coordinate

KMeans
======

.. java:package:: edu.gsu.dmlab.util
   :noindex:

.. java:type:: @SuppressWarnings public class KMeans

Constructors
------------
KMeans
^^^^^^

.. java:constructor:: public KMeans(List<Coordinate> coordinates, Pair<Coordinate, Coordinate> centroids)
   :outertype: KMeans

Methods
-------
getCentroids
^^^^^^^^^^^^

.. java:method:: public Pair<Coordinate, Coordinate> getCentroids()
   :outertype: KMeans

getCoordinates
^^^^^^^^^^^^^^

.. java:method:: public List<Coordinate> getCoordinates()
   :outertype: KMeans

run
^^^

.. java:method:: public List<Cluster> run()
   :outertype: KMeans

setCentroids
^^^^^^^^^^^^

.. java:method:: public void setCentroids(Pair<Coordinate, Coordinate> centroids)
   :outertype: KMeans

setCoordinates
^^^^^^^^^^^^^^

.. java:method:: public void setCoordinates(List<Coordinate> coordinates)
   :outertype: KMeans

