.. java:import:: edu.gsu.dmlab.solgrind.base EventType

InterpolationConstants
======================

.. java:package:: edu.gsu.dmlab.util
   :noindex:

.. java:type:: public class InterpolationConstants

Fields
------
AREAL_I_BUFFER_DISTANCE
^^^^^^^^^^^^^^^^^^^^^^^

.. java:field:: public static final double AREAL_I_BUFFER_DISTANCE
   :outertype: InterpolationConstants

DENSIFIER_POINT_BOUND
^^^^^^^^^^^^^^^^^^^^^

.. java:field:: public static final double DENSIFIER_POINT_BOUND
   :outertype: InterpolationConstants

EPOCH
^^^^^

.. java:field:: public static final long EPOCH
   :outertype: InterpolationConstants

I_INTERVAL
^^^^^^^^^^

.. java:field:: public static final long I_INTERVAL
   :outertype: InterpolationConstants

NUM_CLUSTERS
^^^^^^^^^^^^

.. java:field:: public static final int NUM_CLUSTERS
   :outertype: InterpolationConstants

PERCENTAGE_COORDINATE
^^^^^^^^^^^^^^^^^^^^^

.. java:field:: public static final double PERCENTAGE_COORDINATE
   :outertype: InterpolationConstants

ROUNDING
^^^^^^^^

.. java:field:: public static final RoundingStrategy ROUNDING
   :outertype: InterpolationConstants

SIMPLIFIER_DISTANCE_TOLERANCE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. java:field:: public static final double SIMPLIFIER_DISTANCE_TOLERANCE
   :outertype: InterpolationConstants

TIMESERIES_SIMILARITY_STEPS
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. java:field:: public static final int TIMESERIES_SIMILARITY_STEPS
   :outertype: InterpolationConstants

TS_SCORE_TYPE
^^^^^^^^^^^^^

.. java:field:: public static final DistanceMeasure TS_SCORE_TYPE
   :outertype: InterpolationConstants

Methods
-------
getEventPropagation
^^^^^^^^^^^^^^^^^^^

.. java:method:: public static long getEventPropagation(EventType eventType)
   :outertype: InterpolationConstants

