.. java:import:: edu.gsu.dmlab.solgrind.base EventType

InterpolationConstants.RoundingStrategy
=======================================

.. java:package:: edu.gsu.dmlab.util
   :noindex:

.. java:type:: public enum RoundingStrategy
   :outertype: InterpolationConstants

Enum Constants
--------------
DOWN
^^^^

.. java:field:: public static final InterpolationConstants.RoundingStrategy DOWN
   :outertype: InterpolationConstants.RoundingStrategy

ROUND
^^^^^

.. java:field:: public static final InterpolationConstants.RoundingStrategy ROUND
   :outertype: InterpolationConstants.RoundingStrategy

UP
^^

.. java:field:: public static final InterpolationConstants.RoundingStrategy UP
   :outertype: InterpolationConstants.RoundingStrategy

