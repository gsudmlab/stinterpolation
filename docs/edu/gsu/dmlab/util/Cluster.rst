.. java:import:: java.util ArrayList

.. java:import:: java.util List

.. java:import:: com.vividsolutions.jts.geom Coordinate

Cluster
=======

.. java:package:: edu.gsu.dmlab.util
   :noindex:

.. java:type:: @SuppressWarnings public class Cluster

Fields
------
Coordinates
^^^^^^^^^^^

.. java:field:: public List<Coordinate> Coordinates
   :outertype: Cluster

centroid
^^^^^^^^

.. java:field:: public Coordinate centroid
   :outertype: Cluster

id
^^

.. java:field:: public int id
   :outertype: Cluster

Constructors
------------
Cluster
^^^^^^^

.. java:constructor:: public Cluster(int id)
   :outertype: Cluster

Methods
-------
addCoordinate
^^^^^^^^^^^^^

.. java:method:: @SuppressWarnings public void addCoordinate(Coordinate Coordinate)
   :outertype: Cluster

clear
^^^^^

.. java:method:: public void clear()
   :outertype: Cluster

getCentroid
^^^^^^^^^^^

.. java:method:: public Coordinate getCentroid()
   :outertype: Cluster

getCoordinates
^^^^^^^^^^^^^^

.. java:method:: public List<Coordinate> getCoordinates()
   :outertype: Cluster

getId
^^^^^

.. java:method:: public int getId()
   :outertype: Cluster

plotCluster
^^^^^^^^^^^

.. java:method:: public void plotCluster()
   :outertype: Cluster

setCentroid
^^^^^^^^^^^

.. java:method:: public void setCentroid(Coordinate centroid)
   :outertype: Cluster

setCoordinates
^^^^^^^^^^^^^^

.. java:method:: public void setCoordinates(List Coordinates)
   :outertype: Cluster

