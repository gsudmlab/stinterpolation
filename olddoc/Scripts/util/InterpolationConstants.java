package edu.gsu.dmlab.util;

import edu.gsu.dmlab.solgrind.base.EventType;

public class InterpolationConstants {
	
	public static final long I_INTERVAL = 360000; //in milliseconds (this is 6 minutes)
	
	public static final long EPOCH = 0; //starts at 0, goes to every 10 minutes

	public static final RoundingStrategy ROUNDING = RoundingStrategy.ROUND;
	
	public static final double DENSIFIER_POINT_BOUND = 150;

	public static final int TIMESERIES_SIMILARITY_STEPS = 30;

	public static final DistanceMeasure TS_SCORE_TYPE = DistanceMeasure.DTW;

	public static final double SIMPLIFIER_DISTANCE_TOLERANCE = 1.0;
	
	public static final double PERCENTAGE_COORDINATE=0.2;
	
	public static final int  NUM_CLUSTERS = 2;  
	
	public static final double  AREAL_I_BUFFER_DISTANCE = 1.5;  
	
	public enum RoundingStrategy {
	    UP, DOWN, ROUND
	}
	
	public enum DistanceMeasure{
		DTW, EUCLIDEAN //we can add more if necessary
	}

	public static long getEventPropagation(EventType eventType) {
		if(eventType.equalsIgnoreCase(new EventType("AR")) || eventType.equalsIgnoreCase(new EventType("CH"))){
			return 14400000;
		} else if(eventType.equalsIgnoreCase(new EventType("EF")) ){
			return 9600000;
		} else if(eventType.equalsIgnoreCase(new EventType("FI")) ){
			return 21600000;
		} else if(eventType.equalsIgnoreCase(new EventType("FL")) || eventType.equalsIgnoreCase(new EventType("SS")) ){
			return 600000;
		} else if(eventType.equalsIgnoreCase(new EventType("SG")) ){
			return 5400000;
		} else {
			return I_INTERVAL;
		}
	}

}
