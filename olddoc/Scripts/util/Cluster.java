package edu.gsu.dmlab.util;

import java.util.ArrayList;
import java.util.List;

import com.vividsolutions.jts.geom.Coordinate;
@SuppressWarnings("rawtypes")
public class Cluster {
	
	public List<Coordinate> Coordinates;
	public Coordinate centroid;
	public int id;
	
	//Creates a new Cluster
	public Cluster(int id) {
		this.id = id;
		this.Coordinates = new ArrayList();
		this.centroid = null;
	}

	public List<Coordinate> getCoordinates() {
		return Coordinates;
	}
	
	@SuppressWarnings("unchecked")
	public void addCoordinate(Coordinate Coordinate) {
		Coordinates.add(Coordinate);
	}

	public void setCoordinates(List Coordinates) {
		this.Coordinates = Coordinates;
	}

	public Coordinate getCentroid() {
		return centroid;
	}

	public void setCentroid(Coordinate centroid) {
		this.centroid = centroid;
	}

	public int getId() {
		return id;
	}
	
	public void clear() {
		Coordinates.clear();
	}
	
	public void plotCluster() {
		System.out.println("[Cluster: " + id+"]");
		System.out.println("[Centroid: " + centroid + "]");
		System.out.println("[Coordinates: \n");
		for(int k=0;k<Coordinates.size();k++) {
			System.out.println(Coordinates.get(k).toString());
		}
		System.out.println("]");
	}

}
