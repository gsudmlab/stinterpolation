package edu.gsu.dmlab.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.util.Pair;

import com.vividsolutions.jts.geom.Coordinate;
@SuppressWarnings("rawtypes")
public class KMeans {

	private List<Coordinate> Coordinates;
	private Pair<Coordinate, Coordinate> centroids;
	
    public KMeans(List<Coordinate> coordinates, Pair<Coordinate, Coordinate> centroids) {
		this.Coordinates = coordinates;
		this.centroids = centroids;
	}

    public List<Cluster> run() {
 		return calculate();
 	}
	public List<Coordinate> getCoordinates() {
		return Coordinates;
	}

	public void setCoordinates(List<Coordinate> coordinates) {
		Coordinates = coordinates;
	}

	public Pair<Coordinate, Coordinate> getCentroids() {
		return this.centroids;
	}

	public void setCentroids(Pair<Coordinate, Coordinate> centroids) {
		this.centroids = centroids;
	}

    
    //Initializes the process
    private List<Cluster> init() {
    	//Create Clusters
    	List<Cluster> clusters= new ArrayList<Cluster>();
    	for (int i = 0; i< InterpolationConstants.NUM_CLUSTERS; i++) {
    		Cluster cluster = new Cluster(i);
    		if(i==0) cluster.setCentroid(centroids.getFirst());
    		else cluster.setCentroid(centroids.getSecond());    		
    		clusters.add(cluster);
    	} 	
    	//Print Initial state
    	//plotClusters(clusters);
    	return clusters;
    }

	private void plotClusters(List<Cluster> clusters) {
    	for (int i = 0; i < InterpolationConstants.NUM_CLUSTERS; i++) {
    		Cluster c = (Cluster) clusters.get(i);
    		c.plotCluster();
    	}
    }
    
	//The process to calculate the K Means, with iterating method.
    private List<Cluster> calculate() {
        boolean finish = false;
        int iteration = 0;
     	List<Cluster> clusters = init();
        
        // Add in new data, one at a time, recalculating centroids with each new one. 
        while(!finish) {
        	//Clear cluster state
        	clusters=clearClusters(clusters);
        	
        	List<Coordinate> lastCentroids = new ArrayList<Coordinate>();
        	lastCentroids=getKMeansCentroids(clusters);
        	
        	//Assign Coordinates to the closer cluster
        	clusters=assignCluster(clusters);
           
           //Calculate new centroids.
        	clusters=calculateCentroids(clusters);
        	
        	iteration++;
        	
        	List currentCentroids = getKMeansCentroids(clusters);
        	
        	//Calculates total distance between new and old Centroids
        	double distance = 0;
        	for(int i = 0; i < lastCentroids.size(); i++) {
        		distance += lastCentroids.get(i).distance((Coordinate) currentCentroids.get(i));
        	}
        /*	System.out.println("#################");
        	System.out.println("Iteration: " + iteration);
        	System.out.println("Centroid distances: " + distance);
        	plotClusters(clusters);
        	*/        	
        	if(distance == 0) {
        		finish = true;
        	}
       }
        return clusters;
    }
    
    private List<Cluster> clearClusters(List<Cluster> clusters) {
    	for(int i=0;i<InterpolationConstants.NUM_CLUSTERS;i++) {
    		((Cluster) clusters.get(i)).clear();
    	}
    	return clusters;
    }
    
    private List<Coordinate> getKMeansCentroids(List<Cluster> clusters) {
    	List<Coordinate> centroids = new ArrayList<Coordinate>(InterpolationConstants.NUM_CLUSTERS);
    	for(int i=0;i<InterpolationConstants.NUM_CLUSTERS;i++) {
    		Coordinate aux = ((Cluster) clusters.get(i)).getCentroid();
    		Coordinate Coordinate = new Coordinate(aux.x,aux.y);
    		centroids.add(Coordinate);
    	}
    	return centroids;
    }
    
    private List<Cluster> assignCluster(List<Cluster> clusters) {
        double max = Double.MAX_VALUE;
        double min = max; 
        int cluster = 0;                 
        double distance = 0.0; 
        
        for(int k = 0; k < Coordinates.size(); k++) {
        	Coordinate cor=(Coordinate) Coordinates.get(k);
        	min = max;
            for(int i = 0; i < InterpolationConstants.NUM_CLUSTERS; i++) {
            	Cluster c = (Cluster) clusters.get(i);
                distance = cor.distance(c.getCentroid());
                if(distance < min){
                    min = distance;
                    cluster = i;
                }
            }
            ((Cluster) clusters.get(cluster)).addCoordinate(cor);
        }
        return clusters;
    }

    private List<Cluster> calculateCentroids(List<Cluster> clusters) {
        for(int i = 0; i < InterpolationConstants.NUM_CLUSTERS; i++) {
            double sumX = 0;
            double sumY = 0;
            List<Coordinate> list = ((Cluster) clusters.get(i)).getCoordinates();
            int n_Coordinates = list.size();
            
            for(int j=0;j<list.size();j++){
            	sumX += list.get(j).x;
                sumY += list.get(j).y;
            }
            
            Coordinate centroid = ((Cluster) clusters.get(i)).getCentroid();
            if(n_Coordinates > 0) {
            	double newX = sumX / n_Coordinates;
            	double newY = sumY / n_Coordinates;
                centroid=new Coordinate(newX,newY);
            }
        }
        return clusters;
    }
}