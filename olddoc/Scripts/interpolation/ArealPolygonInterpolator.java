/*////////////////////////////////////////////////////////////////////////
ArealPolygonInterpolatior.java

Copyright (C) 2017  Soukaina Filali Boubrahimi, Berkay Aydin, Dustin Kempton, Rafal Angryk
  Data Mining Laboratory at Georiga State University 
  Principle author contact: sfilaliboubrahimi1@cs.gsu.edu

This file is part of SpatioTemporalInterpolation*.

SpatioTemporalInterpolation* is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; only version 2
of the License. See the COPYING file for more information.
////////////////////////////////////////////////////////////////////*/

package edu.gsu.dmlab.edu.gsu.dmlab.interpolation;
import java.util.ArrayList;
import java.util.TreeSet;

import javax.swing.plaf.synth.SynthSeparatorUI;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;

import edu.gsu.dmlab.solgrind.base.EventType;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.base.types.essential.TGPair;
import edu.gsu.dmlab.solgrind.base.types.essential.Trajectory;
import edu.gsu.dmlab.util.InterpolationConstants;
import edu.gsu.dmlab.util.PosisitonEstimator;



public class ArealPolygonInterpolator extends ComplexPolygonInterpolator{

	@Override
	public Instance interpolate(Instance ins) {		
		System.out.println("\t\t\t\t\tInterpolating instance " + ins.getId() );
	Trajectory iTraj = new Trajectory();
	if(! isValidEventType(ins.getType()) ){
		return null;
	} 
	//interpolate here
	Instance adjusted = Interpolator.adjustTimestamps(ins);
	//System.out.println("\t\tAdjusted::: \n" + adjusted);
	ArrayList<TGPair> tgpList = new ArrayList<TGPair>(adjusted.getTrajectory().getTGPairs());
	for(int i = 0; i < tgpList.size(); i++){
		
		TGPair tgp = tgpList.get(i);
	//	System.out.println("Interpolating... " + tgp);
		TGPair nextTgp = null;
		if (i != tgpList.size()-1){
			nextTgp = tgpList.get(i+1);
		} 
		//System.out.println("Interpolating... InstanceID: " + ins.getId() + " Time: " + tgp.getTInterval());
		TreeSet<TGPair> interpolated = interpolateTGPair(tgp, nextTgp, ins.getType());
		
	//	System.out.println(interpolated.size());
		cleanEmptyPolygons(interpolated);
		
		
		iTraj.getTGPairs().addAll(interpolated);
	}
	iTraj.addTGPair(tgpList.get(tgpList.size()-1));
	Instance iIns = new Instance(ins.getId(), ins.getType(), iTraj);
//	System.out.println("\t\tInterpolated::: \n" + iIns);
	return iIns;
	
	}

	@Override
	public boolean isValidEventType(EventType e) {
		return true;
	}
	
    /**
     * Uses the areal interpolation to interpolated a segment trajectory 
     * of 2 tg-pairs.
     *
     * @param tgp The first tg-pair to be used in the interpolation
     * @param nextTgp The second tg-pair to be used in the interpolation
     * @param time The time to be interpolated in 
     * */
	
	@Override 
	public TGPair interpolateAtTime(TGPair tgp, TGPair nextTgp, long time) {
		//System.out.println("Areal AT TIME");
		TreeSet<TGPair> interpolated = new TreeSet<TGPair>();
		if(nextTgp == null){
			return null;
			}
		
		//start interpolating
		long ts = tgp.getTInterval().getStartTime();
		long te = nextTgp.getTInterval().getStartTime();
		TGPair previousTgp= tgp;
			double lifespan = (double)(te - ts);
			double firstfactor = (double)(time- ts) / lifespan;
			double secondfactor = (double)(lifespan-(time - ts)) / lifespan;
			
			double desiredArea = firstfactor*tgp.getGeometry().getArea() + secondfactor*nextTgp.getGeometry().getArea();
			Geometry interpolatedGeometry;
			TGPair tgpi = null;
				if( time !=(long) ts+lifespan/2){
						interpolatedGeometry=previousTgp.getGeometry();
						interpolatedGeometry = (Polygon) createInterpolatedGeometry( interpolatedGeometry, desiredArea);
					    if(interpolatedGeometry.isEmpty() || interpolatedGeometry==null) interpolatedGeometry=previousTgp.getGeometry();
					    else{
					    	Coordinate[] i_coordinates;
					    	if(time< ts+lifespan/2) i_coordinates = moveCoordinates(interpolatedGeometry, nextTgp.getGeometry(), firstfactor);
					    	else { 
					    		//System.out.println("Areal HERE");
					    		if(interpolatedGeometry.isEmpty() || interpolatedGeometry==null) interpolatedGeometry=previousTgp.getGeometry();
					    		i_coordinates = moveCoordinatesBackward(tgp.getGeometry(),interpolatedGeometry, secondfactor);
					    	}
					    	tgpi = createInterpolatedTGPair(time, time+InterpolationConstants.I_INTERVAL, i_coordinates);
					    }
				}
				else if(time< ts+lifespan/2){
					interpolatedGeometry=tgp.getGeometry();
					interpolatedGeometry = (Polygon) createInterpolatedGeometry( interpolatedGeometry, desiredArea);
				    if(interpolatedGeometry==null) interpolatedGeometry=previousTgp.getGeometry();
				    else{
				    	 Coordinate[] i_coordinates = moveCoordinates(interpolatedGeometry, nextTgp.getGeometry(), firstfactor);
						 tgpi = createInterpolatedTGPair(time, time+InterpolationConstants.I_INTERVAL, i_coordinates);
					}
				}
				else{ 
					interpolatedGeometry=nextTgp.getGeometry();
					interpolatedGeometry = (Polygon) createInterpolatedGeometry( interpolatedGeometry, desiredArea);
			    	if(interpolatedGeometry.isEmpty() || interpolatedGeometry==null) interpolatedGeometry=previousTgp.getGeometry();
				    else{
			    		Coordinate[] i_coordinates = moveCoordinatesBackward(tgp.getGeometry(),interpolatedGeometry, secondfactor);
						tgpi = createInterpolatedTGPair(time, time+InterpolationConstants.I_INTERVAL, i_coordinates);
				    }
				  }
			if(tgpi==null) tgpi=previousTgp;
				interpolated.add(tgpi);
			if(tgpi.getGeometry()!=null)previousTgp=tgpi;
		return interpolated.first();
	}
	
	@Override 
	public TreeSet<TGPair> interpolateTGPair(TGPair tgp, TGPair nextTgp, EventType et) {
	//	System.out.println("Areal");
		TreeSet<TGPair> interpolated = new TreeSet<TGPair>();
		TGPair tgp_i_start = new TGPair(tgp.getTInterval().getStartTime(),
						tgp.getTInterval().getStartTime() + InterpolationConstants.I_INTERVAL,
						tgp.getGeometry(),tgp.getKBarchivID(),false);
		
		if(nextTgp == null){
			PosisitonEstimator pe= new PosisitonEstimator();
			/*Geometry geom= pe.getPredictedPos(p,0)
			nextTgp = new TGPair(tgp.getTInterval().getEndTime(),
								tgp.getTInterval().getEndTime() + 1, 
								tgp.getGeometry(),tgp.getKBarchivID(),false);*/
			//start extrapolating
			long ts = tgp.getTInterval().getStartTime();
			long te = tgp.getTInterval().getEndTime()+ InterpolationConstants.getEventPropagation(et);
			for(long i = ts + InterpolationConstants.I_INTERVAL; i < te; i += InterpolationConstants.I_INTERVAL){
				TGPair tgpi = createInterpolatedTGPair(i, i+InterpolationConstants.I_INTERVAL, pe.getPredictedPos((Polygon)tgp.getGeometry(),(i - ts)).getCoordinates());
				interpolated.add(tgpi);
			} //end of interpolation	
			return interpolated;
		}
		
		interpolated.add(tgp_i_start);//add original to the beginning		
		//start interpolating
		long ts = tgp.getTInterval().getStartTime();
		long te = nextTgp.getTInterval().getStartTime();
		TGPair previousTgp= tgp;
		for(long i = ts + InterpolationConstants.I_INTERVAL; i < te; i += InterpolationConstants.I_INTERVAL){
			double lifespan = (double)(te - ts);
			double firstfactor = (double)(i - ts) / lifespan;
			double secondfactor = (double)(lifespan-(i - ts)) / lifespan;
			
			double desiredArea = firstfactor*tgp.getGeometry().getArea() + secondfactor*nextTgp.getGeometry().getArea();
			Geometry interpolatedGeometry;
			TGPair tgpi = null;
				if( i !=(long) ts+lifespan/2){
						interpolatedGeometry=previousTgp.getGeometry();
						interpolatedGeometry = (Polygon) createInterpolatedGeometry( interpolatedGeometry, desiredArea);
					    if(interpolatedGeometry.isEmpty() || interpolatedGeometry==null) interpolatedGeometry=previousTgp.getGeometry();
					    else{
					    	Coordinate[] i_coordinates;
					    	if(i< ts+lifespan/2) i_coordinates = moveCoordinates(interpolatedGeometry, nextTgp.getGeometry(), firstfactor);
					    	else { 
					    		if(interpolatedGeometry.isEmpty() || interpolatedGeometry==null) interpolatedGeometry=previousTgp.getGeometry();
					    		i_coordinates = moveCoordinatesBackward(tgp.getGeometry(),interpolatedGeometry, secondfactor);
					    	}
					    	tgpi = createInterpolatedTGPair(i, i+InterpolationConstants.I_INTERVAL, i_coordinates);
					    }
				}
				else if(i< ts+lifespan/2){
					interpolatedGeometry=tgp.getGeometry();
					interpolatedGeometry = (Polygon) createInterpolatedGeometry( interpolatedGeometry, desiredArea);
				    if(interpolatedGeometry==null) interpolatedGeometry=previousTgp.getGeometry();
				    else{
				    	 Coordinate[] i_coordinates = moveCoordinates(interpolatedGeometry, nextTgp.getGeometry(), firstfactor);
						 tgpi = createInterpolatedTGPair(i, i+InterpolationConstants.I_INTERVAL, i_coordinates);
					}
				}
				else{ 
					interpolatedGeometry=nextTgp.getGeometry();
					interpolatedGeometry = (Polygon) createInterpolatedGeometry( interpolatedGeometry, desiredArea);
					if(interpolatedGeometry.isEmpty() || interpolatedGeometry==null) interpolatedGeometry=previousTgp.getGeometry();
				    else{
						Coordinate[] i_coordinates = moveCoordinatesBackward(tgp.getGeometry(),interpolatedGeometry, secondfactor);
						tgpi = createInterpolatedTGPair(i, i+InterpolationConstants.I_INTERVAL, i_coordinates);
				    }
				  }
			if(tgpi==null) tgpi=previousTgp;
			 interpolated.add(tgpi);
			
			if(tgpi.getGeometry()!=null)previousTgp=tgpi;
		} //end of interpolation
		return interpolated;
	}
	/**
	 * Create an interpolated geometry based on an initial geometry that
	 * will be buffered to reach a desired area. The buffer distance will be
	 * reduced once the area of the geometry approaches the desired area for better 
	 * accuracy. 
	 * @param interpolatedGeometry
	 * @param desiredArea
	 * @return
	 */
	public Geometry createInterpolatedGeometry(Geometry interpolatedGeometry, double desiredArea) {
		double bufferIncrement;
		if(interpolatedGeometry.getArea()==desiredArea) return interpolatedGeometry;
		else if(interpolatedGeometry.getArea()<desiredArea){ 
			bufferIncrement=InterpolationConstants.AREAL_I_BUFFER_DISTANCE;
			while(interpolatedGeometry.getArea()<desiredArea){
				// To increase accuracy, buffer slowely when the desired area is approched
				if(interpolatedGeometry.getArea()>0.8*desiredArea){ 
					bufferIncrement=0.05;
				}
				interpolatedGeometry= interpolatedGeometry.buffer(bufferIncrement);
				interpolatedGeometry=validateGeometry(interpolatedGeometry);
				}
			}
		else{
			bufferIncrement=-InterpolationConstants.AREAL_I_BUFFER_DISTANCE;
			while(interpolatedGeometry.getArea()>desiredArea){
				// To increase accuracy, increment slowely when the desired area is approched
				if(interpolatedGeometry.getArea()<1.2*desiredArea){ 
					bufferIncrement=-0.05;
				}
				interpolatedGeometry= interpolatedGeometry.buffer(bufferIncrement);
				if(interpolatedGeometry instanceof MultiPolygon){
					interpolatedGeometry=validateGeometry(interpolatedGeometry);
					//The MAX POLYGON from the MULTIPOLYGON is good enough
					if (interpolatedGeometry.getArea()>0.7*desiredArea && interpolatedGeometry.getArea()<desiredArea){
						return interpolatedGeometry;
					}
					// In case the MULTIPOLYGON has been validated by a convex hull startegy
					else if (interpolatedGeometry.getArea()>desiredArea){
						continue;
					}
					else{
						return null;
					}
				}
				else interpolatedGeometry=validateGeometry(interpolatedGeometry);
			}
		}
		return interpolatedGeometry;
	}
}