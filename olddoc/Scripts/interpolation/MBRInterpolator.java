/*////////////////////////////////////////////////////////////////////////
MBRInterpolator.java

Copyright (C) 2017  Soukaina Filali Boubrahimi, Berkay Aydin, Dustin Kempton, Rafal Angryk
  Data Mining Laboratory at Georiga State University 
  Principle author contact: sfilaliboubrahimi1@cs.gsu.edu

This file is part of SpatioTemporalInterpolation*.

SpatioTemporalInterpolation* is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; only version 2
of the License. See the COPYING file for more information.
////////////////////////////////////////////////////////////////////*/
package edu.gsu.dmlab.edu.gsu.dmlab.interpolation;

import java.util.ArrayList;
import java.util.TreeSet;

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;

import edu.gsu.dmlab.solgrind.base.EventType;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.base.types.essential.TGPair;
import edu.gsu.dmlab.solgrind.base.types.essential.TInterval; //DEBUG-ONLY
import edu.gsu.dmlab.solgrind.base.types.essential.Trajectory;
import edu.gsu.dmlab.util.InterpolationConstants;
import edu.gsu.dmlab.util.PosisitonEstimator;

public class MBRInterpolator extends Interpolator{

	GeometryFactory gf = new GeometryFactory();
	
	@Override
	public Instance interpolate(Instance ins) {
		
		Trajectory iTraj = new Trajectory();
		if(! isValidEventType(ins.getType()) ){
			return null;
		} 
		//interpolate here
		Instance adjusted = Interpolator.adjustTimestamps(ins);
//		System.out.println("\t\tAdjusted::: \n" + adjusted);
		ArrayList<TGPair> tgpList = new ArrayList<TGPair>(adjusted.getTrajectory().getTGPairs());
		for(int i = 0; i < tgpList.size(); i++){
			TGPair tgp = tgpList.get(i);
			TGPair nextTgp = null;
			if (i != tgpList.size()-1){
				nextTgp = tgpList.get(i+1);
			} 
			
			TreeSet<TGPair> interpolated = interpolateTGPair(tgp, nextTgp, ins.getType());
			iTraj.getTGPairs().addAll(interpolated);
		}
		Instance iIns = new Instance(ins.getId(), ins.getType(), iTraj);
//		System.out.println("\t\tInterpolated::: \n" + iIns);
		return iIns;
	}

	@Override
	public TreeSet<TGPair> interpolateTGPair(TGPair tgp, TGPair nextTgp, EventType et) {
		TreeSet<TGPair> interpolated = new TreeSet<TGPair>();
		TGPair tgp_i_start = new TGPair(tgp.getTInterval().getStartTime(),
						tgp.getTInterval().getStartTime() + InterpolationConstants.I_INTERVAL,
						tgp.getGeometry(),tgp.getKBarchivID(),false);
		System.out.println("KB Arhiv id is "+ tgp.getKBarchivID() );
		interpolated.add(tgp_i_start);
		
		if(nextTgp == null){
			PosisitonEstimator pe= new PosisitonEstimator();
			/*Geometry geom= pe.getPredictedPos(p,0)
			nextTgp = new TGPair(tgp.getTInterval().getEndTime(),
								tgp.getTInterval().getEndTime() + 1, 
								tgp.getGeometry(),tgp.getKBarchivID(),false);*/
			//start interpolating
			long ts = tgp.getTInterval().getStartTime();
			long te = tgp.getTInterval().getEndTime() + InterpolationConstants.getEventPropagation(et);

			for(long i = ts + InterpolationConstants.I_INTERVAL; i < te; i += InterpolationConstants.I_INTERVAL){
				Geometry geom = pe.getPredictedPos((Polygon)tgp.getGeometry(),(i - ts));
				Envelope env = geom.getEnvelopeInternal();
				double maxX_0 = env.getMaxX();
				double maxY_0 = env.getMaxY();
				double minX_0 = env.getMinX();
				double minY_0 = env.getMinY();
				TGPair tgpi = createInterpolatedTGPair(i, i+InterpolationConstants.I_INTERVAL,maxX_0, minX_0, maxY_0, minY_0); 
						
				interpolated.add(tgpi);
			} //end of interpolation	
			return interpolated;
		}

		long ts = tgp.getTInterval().getStartTime();
		long te = nextTgp.getTInterval().getStartTime();
		Envelope env = tgp.getGeometry().getEnvelopeInternal();
		double maxX_0 = env.getMaxX();
		double maxY_0 = env.getMaxY();
		double minX_0 = env.getMinX();
		double minY_0 = env.getMinY();
		Envelope nextEnv = nextTgp.getGeometry().getEnvelopeInternal();
		double maxX_n = nextEnv.getMaxX();
		double maxY_n = nextEnv.getMaxY();
		double minX_n = nextEnv.getMinX();
		double minY_n = nextEnv.getMinY();
		
		for(long i = ts + InterpolationConstants.I_INTERVAL; i < te; 
				i += InterpolationConstants.I_INTERVAL){
			//System.out.println( TInterval.convertLongToTimestamp(i) ); //DEBUG-ONLY
			
			double factor = (double)(i - ts) / (double)(te - ts);
			//System.out.println(factor); //DEBUG-ONLY
			
			double maxX_i = linearInterpolate(maxX_0, maxX_n, factor);
			double minX_i = linearInterpolate(minX_0, minX_n, factor);
			double maxY_i = linearInterpolate(maxY_0, maxY_n, factor);
			double minY_i = linearInterpolate(minY_0, minY_n, factor);
			
			TGPair tgpi = createInterpolatedTGPair(i, i+InterpolationConstants.I_INTERVAL, maxX_i, minX_i, maxY_i, minY_i);
			//System.out.println(tgpi.getGeometry().toString());
			interpolated.add(tgpi);
		}
		return interpolated;
	}

	/**
	 * Linearly interpolates a 1D variable (between x_0 and x_k). 
	 * factor is the step size
	 * Say you have x_0 = 1, x_k = 11, factor=0.4, your interpolated result will be 5
	 * @param x_0 - minimum bound
	 * @param x_k - maximum bound
	 * @param factor - the factor of linear interpolation (factor must be between 0 and 1)
	 * @return - interpolated result
	 */
	private double linearInterpolate(double x_0, double x_k, double factor) {
		if(factor <0.0 || factor > 1.0){
			return Double.NaN;
		} else{
			return x_0 + ((x_k - x_0) * factor );
		}
		
	}

	/**
	 * Creates the TGPair object using given MBR min max points and start and end times.
	 * @param startTime
	 * @param endTime
	 * @param maxX_i
	 * @param minX_i
	 * @param maxY_i
	 * @param minY_i
	 * @return
	 */
	private TGPair createInterpolatedTGPair(long startTime, long endTime, double maxX_i, double minX_i, double maxY_i, double minY_i) {
		Envelope envI = new Envelope(maxX_i, minX_i, maxY_i, minY_i);
		Geometry polygonI = gf.toGeometry(envI);
		return new TGPair(startTime, endTime, polygonI,null,true);
	}

	@Override
	public boolean isValidEventType(EventType e) {
		return e.getType().equalsIgnoreCase("EF") || e.getType().equalsIgnoreCase("SG") 
				|| e.getType().equalsIgnoreCase("FL");
	}

	@Override
	public TGPair interpolateAtTime(TGPair tgp, TGPair nextTgp, long time) {
		if(nextTgp == null){	
			return null;
		}

		long ts = tgp.getTInterval().getStartTime();
		long te = nextTgp.getTInterval().getStartTime();
		Envelope env = tgp.getGeometry().getEnvelopeInternal();
		double maxX_0 = env.getMaxX();
		double maxY_0 = env.getMaxY();
		double minX_0 = env.getMinX();
		double minY_0 = env.getMinY();
		Envelope nextEnv = nextTgp.getGeometry().getEnvelopeInternal();
		double maxX_n = nextEnv.getMaxX();
		double maxY_n = nextEnv.getMaxY();
		double minX_n = nextEnv.getMinX();
		double minY_n = nextEnv.getMinY();
		
			
			double factor = (double)(time - ts) / (double)(te - ts);
			double maxX_i = linearInterpolate(maxX_0, maxX_n, factor);
			double minX_i = linearInterpolate(minX_0, minX_n, factor);
			double maxY_i = linearInterpolate(maxY_0, maxY_n, factor);
			double minY_i = linearInterpolate(minY_0, minY_n, factor);
			
			TGPair tgpi = createInterpolatedTGPair(time, time+InterpolationConstants.I_INTERVAL, maxX_i, minX_i, maxY_i, minY_i);
		return tgpi;
	}
}
