/**
 * 
 */
package edu.gsu.dmlab.solgrind.base.types.event;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.gsu.dmlab.solgrind.base.EventType;

/**
 * This class is will be used for ST co-occurrence pattern.
 * (Naming is EventCooccurrence because not every set of 
 * event co-occurrences are necessarily frequent patterns)
 * @author berkay - Jul 12, 2016
 */
public class EventCooccurrence implements Comparable<EventCooccurrence>{
	
	private Set<EventType> eventTypes;
	
	public EventCooccurrence( Collection<EventType> eventTypes ){
		this.eventTypes = new TreeSet<EventType>(eventTypes);
	}
	
	public EventCooccurrence(){
		eventTypes = new TreeSet<>();
	}
	
	/** copy constructor
	 * @param eventCooccurrence
	 */
	public EventCooccurrence(EventCooccurrence eventCooccurrence) {
		eventTypes = new TreeSet<>();
		for(EventType otherEvent : eventCooccurrence.getEventTypes()){
			eventTypes.add(new EventType(otherEvent.getType()));
		}
	}

	public void addEventType(EventType eventType) {
		this.eventTypes.add(eventType);
	}
	
	public Set<EventType> getEventTypes(){
		return eventTypes;
	}
	
	public String toString(){
		return "EventCO:"+this.eventTypes.toString();
	}
	
	public int getCardinality(){
		return this.eventTypes.size();
	}

	public boolean equals(Object et){
		if(et instanceof EventCooccurrence){
			if(this != null && et != null){
				return this.eventTypes.containsAll((Collection<?>) et);
			} else{ //at least one of the ECs is null
				return false;
			}
		} else{
			return false;
		}
	}
	
	public int hashCode() {
		int i = 13;
		if( this != null ){
			for(EventType e : this.eventTypes){
				i += e.hashCode();
			}
		}
		return i;
	}

	public EventCooccurrence union(EventCooccurrence eventCooccurrence) {
		SortedSet<EventType> es1 = new TreeSet<EventType>(eventTypes);
		SortedSet<EventType> es2 = new TreeSet<EventType>(eventCooccurrence.getEventTypes());
		es1.addAll(es2);
		return new EventCooccurrence(es1);
	}

	@Override
	public int compareTo(EventCooccurrence o) {
		if(this.getCardinality() > o.getCardinality()){
			return -1;
		} else if(this.getCardinality() < o.getCardinality()){
			return 1;
		} else{
			return this.eventTypes.toString().compareTo(o.eventTypes.toString());
		}
	}

}
