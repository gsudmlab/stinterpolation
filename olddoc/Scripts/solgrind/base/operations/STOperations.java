package edu.gsu.dmlab.solgrind.base.operations;

import java.util.Collection;
import java.util.TreeSet;

import com.vividsolutions.jts.geom.Geometry;

import edu.gsu.dmlab.solgrind.base.types.essential.TGPair;
import edu.gsu.dmlab.solgrind.base.types.essential.TInterval;
import edu.gsu.dmlab.solgrind.base.types.essential.Trajectory;

/**
 * Implements spatiotemporal operations
 * 
 * @author berkay - Aug 1, 2016
 *
 */

public class STOperations {

	public static Trajectory union(Trajectory traj1, Trajectory traj2) {
		Trajectory trj = new Trajectory();
		
		if( !TOperations.tIntersects(traj1, traj2) ){  //do the simple union
			trj.getTGPairs().addAll(traj1.getTGPairs());
			trj.getTGPairs().addAll(traj2.getTGPairs());
			return trj; 
		}
		Trajectory earlyTrajectory = traj1;
		Trajectory lateTrajectory = traj2;
		if(traj1.getStartTime() > traj2.getStartTime()){
			//traj2 starts earlier
			earlyTrajectory = traj2;
			lateTrajectory = traj1;
		}
		
		for( TGPair earlyPair : earlyTrajectory.getTGPairs() ){ //get early trajectory's pair
			TInterval earlyTI = earlyPair.getTInterval(); //early traj pair's time interval
			TreeSet<TGPair> lateTGPairs = lateTrajectory.getSegment(earlyTI.getStartTime(), earlyTI.getEndTime()).getTGPairs();
			
			Geometry earlyGeom = earlyPair.getGeometry();
			for(TGPair latePair :lateTGPairs){ //late trajectory's pair
				TInterval t2ti = latePair.getTInterval();
				Geometry t2geom = latePair.getGeometry();
				Geometry uGeometry = earlyGeom.union(t2geom);
				trj.addTGPair(t2ti.getStartTime(), t2ti.getEndTime(), uGeometry);
			}
		}
		
		//check if late trajectory has some more tg pairs that are not covered above
		if( earlyTrajectory.getEndTime() <= lateTrajectory.getEndTime() ){
			Trajectory lateLastParts = lateTrajectory.getSegment(earlyTrajectory.getEndTime(), lateTrajectory.getEndTime());
			trj.getTGPairs().addAll(lateLastParts.getTGPairs());
		}
		
		return trj;
	}

	public static Trajectory intersection(Trajectory traj1, Trajectory traj2){
		
		
		Trajectory trj = new Trajectory();
		if( !TOperations.tIntersects(traj1, traj2) ){
			return trj; //return empty
		}
		
		if( !SOperations.sIntersectsMBR(traj1, traj2) ){
			return trj; //return empty
		}
		
		
		for( TGPair t1Pair : traj1.getTGPairs() ){
			TInterval t1ti = t1Pair.getTInterval(); //t1 pair's time interval
			TreeSet<TGPair> tgpairs2 = traj2.getSegment(t1ti.getStartTime(), t1ti.getEndTime()).getTGPairs();
			
			if(tgpairs2.size() == 0){
				continue;
			}
			
			Geometry t1geom = t1Pair.getGeometry();
			for(TGPair t2pair :tgpairs2){
				
				TInterval t2ti = t2pair.getTInterval();
				Geometry t2geom = t2pair.getGeometry();
				Geometry iGeometry = t1geom.intersection(t2geom);
				trj.addTGPair(t2ti.getStartTime(), t2ti.getEndTime(), iGeometry);
			}
		}
		return trj;
	}
	
	public static boolean stIntersects(Trajectory traj1, Trajectory traj2){
		
		Trajectory trj = new Trajectory();
		if( !TOperations.tIntersects(traj1, traj2) ){
			return false; //return false
		}
		
		if( !SOperations.sIntersectsMBR(traj1, traj2) ){
			return false; //return false
		}
		
		
		for( TGPair t1Pair : traj1.getTGPairs() ){
			TInterval t1ti = t1Pair.getTInterval(); //t1 pair's time interval
			TreeSet<TGPair> tgpairs2 = traj2.getSegment(t1ti.getStartTime(), t1ti.getEndTime()).getTGPairs();
			
			if(tgpairs2.size() == 0){
				continue;
			}
			
			Geometry t1geom = t1Pair.getGeometry();
			for(TGPair t2pair :tgpairs2){
				TInterval t2ti = t2pair.getTInterval();
				Geometry t2geom = t2pair.getGeometry();
				if(t1geom.intersects(t2geom)){
					return true;
				}
			}
		}
		return false;
	}

	public static Trajectory unionAll(Collection<Trajectory> trajCollection) {

		return new Trajectory();
	}

	public static Trajectory intersectionAll(Collection<Trajectory> trajCollection) {

		return new Trajectory();
	}

	public static Geometry interpolate(Geometry geometry, Geometry nextGeometry, double samplingRatio) {
		return geometry;
	}

	public static Geometry interpolate(Geometry geometry, long l) {
		return geometry;
	}

}
