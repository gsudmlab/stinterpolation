Introduction
============

Why do you need Spatiotemporal Interpolation?
------------------------------

Although a tremendous amount of solar data now exists, one of the main challenges that 
researchers face is the limited amount of accurately labeled data.
Another impact of missing data is on the quality of solar data mining results, 
specifically that of spatiotemporal frequent pattern mining studies. The richness of vector-based solar data is, therefore,
crucial for both solar physics and solar data mining communities. To solve this problem, we
propose four automated interpolation methods that are MBR-Interpolation (Minimum
Bounding Rectangle Interpolation), CP-Interpolation (Complex Polygon
Interpolation), FI-Interpolation (Filament Polygon Interpolation) and Areal-Interpolation.

The interpolation modules takes a trajectory made
of two time-geometry pairs as input and generates an enriched trajectory containing
interpolated time-geometry pairs. The task of interpolating the missing event instances consists of
estimating the location of the event at times when the vector data is not available, therefore creating a
more enriched dataset with broader coverage and uniformly reported events
for querying and analysis.


What is MBR Interpolation?
-------------------------------

MBR-Interpolation technique is used when the only spatial information available
for event instances is their MBR representation. Those event types are Emerging
Flux, Flares, and Sigmoids. The MBR-Interpolation is the simplest technique as
it involves only matching two points and the standard linear interpolation
technique is then applied.

.. figure:: ../images/MBRInter.png
   :scale: 70 %
   :alt: MBRInterpolation
   :align: center

What is Complex Polygon Interpolation?
-------------------------------

CP-Interpolation is the most generitc type of interpolation. It starts with transforming
the polygon-based region geometries into shape signatures.The values of the shape signature
represent the distance between the centroid of the region geometry and the point
coordinates that constitute the polygon boundary. We use the Dynamic Time Warping (DTW) 
technique to align the two input polygons represented as shape signatures.
The result of DTW is a many-to-many point matching of the
values of the two shape signatures. Linear interpolation is then applied between the matched 
pair of points to generate the interpolated polygons. The algorithm that we have developed is
composed of four main parts, which are: (1)~shape signature
polygon transformation, (2)~sliding window search, (3)~points matching, 
and (4)~linear interpolation.

.. image:: ../images/CPInterpolation.png
   :scale: 70 %
   :alt: CP-Interpolation
   :align: center

What is Filamet Polygon Interpolation?
-------------------------------

FI-Interpolation algorithm is specialized for the solar filament event type. It
starts by detecting the endpoints, also known as the filament feet, which
is one of the prominent spatial characteristics of filament events. The two
input filament polygons are then transformed into two shape signatures. The
values of the shape signature represent the distance between the centroid of
the filament geometry and the point coordinates that make the boundary of the
polygon. In order to align the two input filament polygons represented as shape
signatures, DTW is used in a similar way that it was used in CP-Interpolation.

.. image:: ../images/FInterpolation.png
   :scale: 70 %
   :alt: CP-Interpolation
   :align: center
   
What is Areal Interpolation?
-------------------------------

Areal interpolation follows a simpler procedure.
The method starts from the assumption that the area of the geometry to be
interpolated ranges between the areas of the start and end
trajectory geometries. To deduce the interpolated area, we applied linear
interpolation on the areas of the two input geometries with respect to the
corresponding interpolation time, that we call the \emph{desired
area}. The next step consists of choosing the input geometry with
the closest temporal proximity that we will later transform into the
interpolated one. 

.. figure:: ../images/ArealInter.png
   :scale: 70 %
   :alt: ArealInter
   :align: center

What else is out there?
-----------------------

In addition to the interpolation, the input trajectories are further extrapolated 
for a period of time T that differs from one solar event to another. The extrapolation
predicts the position of the event in future times based on the solar rotation parameter.