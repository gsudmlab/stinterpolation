.. java:import:: com.vividsolutions.jts.geom Envelope

.. java:import:: com.vividsolutions.jts.geom Geometry

.. java:import:: edu.gsu.dmlab.solgrind.base.operations STOperations

.. java:import:: java.util Collection

.. java:import:: java.util Iterator

.. java:import:: java.util SortedSet

.. java:import:: java.util TreeSet

Trajectory
==========

.. java:package:: edu.gsu.dmlab.solgrind.base.types.essential
   :noindex:

.. java:type:: public class Trajectory

   :author: berkay - Jul 12, 2016

Constructors
------------
Trajectory
^^^^^^^^^^

.. java:constructor:: public Trajectory()
   :outertype: Trajectory

Trajectory
^^^^^^^^^^

.. java:constructor:: public Trajectory(Collection<TGPair> timeGeometryPairs)
   :outertype: Trajectory

Methods
-------
addTGPair
^^^^^^^^^

.. java:method:: public void addTGPair(long startTime, long endTime, Geometry geometry)
   :outertype: Trajectory

addTGPair
^^^^^^^^^

.. java:method:: public void addTGPair(TGPair tgp)
   :outertype: Trajectory

getEndTime
^^^^^^^^^^

.. java:method:: public long getEndTime()
   :outertype: Trajectory

getMBR
^^^^^^

.. java:method:: public Envelope getMBR()
   :outertype: Trajectory

getSegment
^^^^^^^^^^

.. java:method:: public Trajectory getSegment(long start, long end)
   :outertype: Trajectory

getStartTime
^^^^^^^^^^^^

.. java:method:: public long getStartTime()
   :outertype: Trajectory

getTGPairSize
^^^^^^^^^^^^^

.. java:method:: public int getTGPairSize()
   :outertype: Trajectory

getTGPairs
^^^^^^^^^^

.. java:method:: public TreeSet<TGPair> getTGPairs()
   :outertype: Trajectory

getTimeIntervals
^^^^^^^^^^^^^^^^

.. java:method:: public TreeSet<TInterval> getTimeIntervals()
   :outertype: Trajectory

getVolume
^^^^^^^^^

.. java:method:: public double getVolume()
   :outertype: Trajectory

isEmpty
^^^^^^^

.. java:method:: public boolean isEmpty()
   :outertype: Trajectory

isValid
^^^^^^^

.. java:method:: public boolean isValid()
   :outertype: Trajectory

sampleTGPairs
^^^^^^^^^^^^^

.. java:method:: public Trajectory sampleTGPairs(Trajectory traj, long samplingInterval)
   :outertype: Trajectory

   Based on a sampling interval, sample the time-geometry pairs in the trajectory

   :param tailBuffer:
   :param samplingInterval:

toString
^^^^^^^^

.. java:method:: public String toString()
   :outertype: Trajectory

