.. java:import:: java.sql Timestamp

.. java:import:: java.util Set

SamplingPeriodException
=======================

.. java:package:: edu.gsu.dmlab.solgrind.base.types.essential
   :noindex:

.. java:type::  class SamplingPeriodException extends Exception

Constructors
------------
SamplingPeriodException
^^^^^^^^^^^^^^^^^^^^^^^

.. java:constructor:: public SamplingPeriodException(String message)
   :outertype: SamplingPeriodException

SamplingPeriodException
^^^^^^^^^^^^^^^^^^^^^^^

.. java:constructor:: public SamplingPeriodException(String message, Throwable throwable)
   :outertype: SamplingPeriodException

