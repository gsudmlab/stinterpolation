.. java:import:: java.sql Timestamp

.. java:import:: java.util Set

TInterval
=========

.. java:package:: edu.gsu.dmlab.solgrind.base.types.essential
   :noindex:

.. java:type:: public class TInterval implements Comparable<TInterval>

   Class for handling time intervals in trajectories

   :author: berkay - Jul 15, 2016

Fields
------
PERIOD_SET
^^^^^^^^^^

.. java:field::  boolean PERIOD_SET
   :outertype: TInterval

Constructors
------------
TInterval
^^^^^^^^^

.. java:constructor:: public TInterval(long start, long end)
   :outertype: TInterval

TInterval
^^^^^^^^^

.. java:constructor:: public TInterval(long start) throws SamplingPeriodException
   :outertype: TInterval

TInterval
^^^^^^^^^

.. java:constructor:: public TInterval(String startTimestamp, String endTimestamp)
   :outertype: TInterval

TInterval
^^^^^^^^^

.. java:constructor:: public TInterval(String startTimestamp) throws SamplingPeriodException
   :outertype: TInterval

Methods
-------
compareTo
^^^^^^^^^

.. java:method:: @Override public int compareTo(TInterval tI)
   :outertype: TInterval

contains
^^^^^^^^

.. java:method:: public boolean contains(long timePoint)
   :outertype: TInterval

convertLongToTimestamp
^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public static String convertLongToTimestamp(long time)
   :outertype: TInterval

convertTimestampToLong
^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public static long convertTimestampToLong(String timestampString)
   :outertype: TInterval

endsAfter
^^^^^^^^^

.. java:method:: public boolean endsAfter(TInterval interval)
   :outertype: TInterval

endsBefore
^^^^^^^^^^

.. java:method:: public boolean endsBefore(TInterval interval)
   :outertype: TInterval

endsBeforeStartOf
^^^^^^^^^^^^^^^^^

.. java:method:: public boolean endsBeforeStartOf(TInterval interval)
   :outertype: TInterval

equals
^^^^^^

.. java:method:: @Override public boolean equals(Object obj)
   :outertype: TInterval

getEndTime
^^^^^^^^^^

.. java:method:: public long getEndTime()
   :outertype: TInterval

getEndTimeString
^^^^^^^^^^^^^^^^

.. java:method:: public String getEndTimeString()
   :outertype: TInterval

getLifespan
^^^^^^^^^^^

.. java:method:: public double getLifespan()
   :outertype: TInterval

getStartTime
^^^^^^^^^^^^

.. java:method:: public long getStartTime()
   :outertype: TInterval

getStartTimeString
^^^^^^^^^^^^^^^^^^

.. java:method:: public String getStartTimeString()
   :outertype: TInterval

hashCode
^^^^^^^^

.. java:method:: @Override public int hashCode()
   :outertype: TInterval

intersection
^^^^^^^^^^^^

.. java:method:: public TInterval intersection(TInterval desiredTInterval)
   :outertype: TInterval

isIntervalPeriodSet
^^^^^^^^^^^^^^^^^^^

.. java:method:: public boolean isIntervalPeriodSet()
   :outertype: TInterval

isValid
^^^^^^^

.. java:method:: public boolean isValid()
   :outertype: TInterval

overlaps
^^^^^^^^

.. java:method:: public boolean overlaps(TInterval timeInterval)
   :outertype: TInterval

setSamplingPeriod
^^^^^^^^^^^^^^^^^

.. java:method:: public void setSamplingPeriod(long period)
   :outertype: TInterval

startsAfter
^^^^^^^^^^^

.. java:method:: public boolean startsAfter(TInterval interval)
   :outertype: TInterval

startsBefore
^^^^^^^^^^^^

.. java:method:: public boolean startsBefore(TInterval timeInterval)
   :outertype: TInterval

toString
^^^^^^^^

.. java:method:: public String toString()
   :outertype: TInterval

