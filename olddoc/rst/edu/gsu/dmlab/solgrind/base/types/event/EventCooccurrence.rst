.. java:import:: java.util ArrayList

.. java:import:: java.util Collection

.. java:import:: java.util Set

.. java:import:: java.util SortedSet

.. java:import:: java.util TreeSet

.. java:import:: edu.gsu.dmlab.solgrind.base EventType

EventCooccurrence
=================

.. java:package:: edu.gsu.dmlab.solgrind.base.types.event
   :noindex:

.. java:type:: public class EventCooccurrence implements Comparable<EventCooccurrence>

   This class is will be used for ST co-occurrence pattern. (Naming is EventCooccurrence because not every set of event co-occurrences are necessarily frequent patterns)

   :author: berkay - Jul 12, 2016

Constructors
------------
EventCooccurrence
^^^^^^^^^^^^^^^^^

.. java:constructor:: public EventCooccurrence(Collection<EventType> eventTypes)
   :outertype: EventCooccurrence

EventCooccurrence
^^^^^^^^^^^^^^^^^

.. java:constructor:: public EventCooccurrence()
   :outertype: EventCooccurrence

EventCooccurrence
^^^^^^^^^^^^^^^^^

.. java:constructor:: public EventCooccurrence(EventCooccurrence eventCooccurrence)
   :outertype: EventCooccurrence

   copy constructor

   :param eventCooccurrence:

Methods
-------
addEventType
^^^^^^^^^^^^

.. java:method:: public void addEventType(EventType eventType)
   :outertype: EventCooccurrence

compareTo
^^^^^^^^^

.. java:method:: @Override public int compareTo(EventCooccurrence o)
   :outertype: EventCooccurrence

equals
^^^^^^

.. java:method:: public boolean equals(Object et)
   :outertype: EventCooccurrence

getCardinality
^^^^^^^^^^^^^^

.. java:method:: public int getCardinality()
   :outertype: EventCooccurrence

getEventTypes
^^^^^^^^^^^^^

.. java:method:: public Set<EventType> getEventTypes()
   :outertype: EventCooccurrence

hashCode
^^^^^^^^

.. java:method:: public int hashCode()
   :outertype: EventCooccurrence

toString
^^^^^^^^

.. java:method:: public String toString()
   :outertype: EventCooccurrence

union
^^^^^

.. java:method:: public EventCooccurrence union(EventCooccurrence eventCooccurrence)
   :outertype: EventCooccurrence

