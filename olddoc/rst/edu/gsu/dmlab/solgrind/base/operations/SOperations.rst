.. java:import:: com.vividsolutions.jts.geom Envelope

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential Trajectory

SOperations
===========

.. java:package:: edu.gsu.dmlab.solgrind.base.operations
   :noindex:

.. java:type:: public class SOperations

Methods
-------
sIntersectsMBR
^^^^^^^^^^^^^^

.. java:method:: public static boolean sIntersectsMBR(Trajectory traj1, Trajectory traj2)
   :outertype: SOperations

