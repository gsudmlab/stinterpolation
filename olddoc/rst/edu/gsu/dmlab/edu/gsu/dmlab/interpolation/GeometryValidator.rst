.. java:import:: java.util Collection

.. java:import:: java.util Iterator

.. java:import:: com.vividsolutions.jts.geom Geometry

.. java:import:: com.vividsolutions.jts.geom GeometryFactory

.. java:import:: com.vividsolutions.jts.geom LineString

.. java:import:: com.vividsolutions.jts.geom LinearRing

.. java:import:: com.vividsolutions.jts.geom MultiPolygon

.. java:import:: com.vividsolutions.jts.geom Point

.. java:import:: com.vividsolutions.jts.geom Polygon

.. java:import:: com.vividsolutions.jts.operation.polygonize Polygonizer

.. java:import:: com.vividsolutions.jts.simplify DouglasPeuckerSimplifier

.. java:import:: edu.gsu.dmlab.util InterpolationConstants

GeometryValidator
=================

.. java:package:: edu.gsu.dmlab.edu.gsu.dmlab.interpolation
   :noindex:

.. java:type:: public class GeometryValidator

Methods
-------
addLineString
^^^^^^^^^^^^^

.. java:method:: static void addLineString(LineString lineString, Polygonizer polygonizer)
   :outertype: GeometryValidator

   Add the linestring given to the polygonizer

   :param linestring: line string
   :param polygonizer: polygonizer

addPolygon
^^^^^^^^^^

.. java:method:: static void addPolygon(Polygon polygon, Polygonizer polygonizer)
   :outertype: GeometryValidator

   Add all line strings from the polygon given to the polygonizer given

   :param polygon: polygon from which to extract line strings
   :param polygonizer: polygonizer

bufferValidate
^^^^^^^^^^^^^^

.. java:method:: public static Geometry bufferValidate(Geometry geom)
   :outertype: GeometryValidator

   Validate given geometry using buffer(0.0) trick

   :param geom:

polygonizerValidate
^^^^^^^^^^^^^^^^^^^

.. java:method:: public static Geometry polygonizerValidate(Geometry geom)
   :outertype: GeometryValidator

   Get / create a valid version of the geometry given. If the geometry is a polygon or multi polygon, self intersections / inconsistencies are fixed. Otherwise the geometry is returned.

   :param geom:
   :return: a geometry

simplifierValidate
^^^^^^^^^^^^^^^^^^

.. java:method:: public static Geometry simplifierValidate(Geometry geom)
   :outertype: GeometryValidator

   Validate the given polygon-based geometry using D-P simplifier setEnsureValid is set to true

   :param geom:

toPolygonGeometry
^^^^^^^^^^^^^^^^^

.. java:method:: static Geometry toPolygonGeometry(Collection<Polygon> polygons, GeometryFactory factory)
   :outertype: GeometryValidator

   Get a geometry from a collection of polygons.

   :param polygons: collection
   :param factory: factory to generate MultiPolygon if required
   :return: null if there were no polygons, the polygon if there was only one, or a MultiPolygon containing all polygons otherwise

validateGeometry
^^^^^^^^^^^^^^^^

.. java:method:: public static Geometry validateGeometry(Geometry geom)
   :outertype: GeometryValidator

   Validates the given polygon-based geometry. Tries to simplify the geometry. Simplification is used for multipolygon elimination. This is much experimental, use it at your own risk.

   :param geom: -- polygon-based geometry

