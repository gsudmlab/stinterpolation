.. java:import:: java.util AbstractMap

.. java:import:: java.util ArrayList

.. java:import:: java.util HashMap

.. java:import:: java.util Map

.. java:import:: java.util Map.Entry

.. java:import:: java.util TreeMap

.. java:import:: java.util TreeSet

.. java:import:: com.vividsolutions.jts.geom Coordinate

.. java:import:: com.vividsolutions.jts.geom Geometry

.. java:import:: com.vividsolutions.jts.geom GeometryCollection

.. java:import:: com.vividsolutions.jts.geom GeometryFactory

.. java:import:: com.vividsolutions.jts.geom MultiPolygon

.. java:import:: com.vividsolutions.jts.geom Point

.. java:import:: com.vividsolutions.jts.geom Polygon

.. java:import:: com.vividsolutions.jts.geom TopologyException

.. java:import:: com.vividsolutions.jts.simplify DouglasPeuckerSimplifier

.. java:import:: edu.gsu.dmlab.solgrind.base EventType

.. java:import:: edu.gsu.dmlab.solgrind.base Instance

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential TGPair

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential Trajectory

.. java:import:: edu.gsu.dmlab.util InterpolationConstants

.. java:import:: edu.gsu.dmlab.util PosisitonEstimator

.. java:import:: net.sf.javaml.distance.fastdtw.dtw FastDTW

.. java:import:: net.sf.javaml.distance.fastdtw.dtw TimeWarpInfo

.. java:import:: net.sf.javaml.distance.fastdtw.matrix ColMajorCell

.. java:import:: net.sf.javaml.distance.fastdtw.timeseries TimeSeries

.. java:import:: net.sf.javaml.distance.fastdtw.timeseries TimeSeriesPoint

ComplexPolygonInterpolator
==========================

.. java:package:: edu.gsu.dmlab.edu.gsu.dmlab.interpolation
   :noindex:

.. java:type:: public class ComplexPolygonInterpolator extends Interpolator

Fields
------
arealInvalid_count
^^^^^^^^^^^^^^^^^^

.. java:field:: public static int arealInvalid_count
   :outertype: ComplexPolygonInterpolator

empty_count
^^^^^^^^^^^

.. java:field:: public static int empty_count
   :outertype: ComplexPolygonInterpolator

Methods
-------
arealValidation
^^^^^^^^^^^^^^^

.. java:method:: protected boolean arealValidation(TreeSet<TGPair> interpolated, TGPair tgp, TGPair nextTgp)
   :outertype: ComplexPolygonInterpolator

cleanEmptyPolygons
^^^^^^^^^^^^^^^^^^

.. java:method:: static TreeSet<TGPair> cleanEmptyPolygons(TreeSet<TGPair> interpolated)
   :outertype: ComplexPolygonInterpolator

convertToTimeseries
^^^^^^^^^^^^^^^^^^^

.. java:method:: public static TimeSeries convertToTimeseries(Geometry geom)
   :outertype: ComplexPolygonInterpolator

createInterpolatedCoordinates
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: static Coordinate[] createInterpolatedCoordinates(Geometry densifiedGeom, Geometry densifiedNextGeom, TimeWarpInfo warpInfo, double factor)
   :outertype: ComplexPolygonInterpolator

createInterpolatedTGPair
^^^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: static TGPair createInterpolatedTGPair(long startTime, long endTime, Coordinate[] i_coordinates)
   :outertype: ComplexPolygonInterpolator

createOffsetTimeSeriesMap
^^^^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public static HashMap<Integer, TimeSeries> createOffsetTimeSeriesMap(Geometry densifiedNextGeom)
   :outertype: ComplexPolygonInterpolator

   :param densifiedNextGeom:

getKthMostSimilarWarpPathInfo
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public static Entry<Integer, TimeWarpInfo> getKthMostSimilarWarpPathInfo(TimeSeries tgpTS, HashMap<Integer, TimeSeries> offsetTimeSeriesMap, int k)
   :outertype: ComplexPolygonInterpolator

getWarpInfo
^^^^^^^^^^^

.. java:method:: public static TimeWarpInfo getWarpInfo(TimeSeries tsI, TimeSeries tsJ)
   :outertype: ComplexPolygonInterpolator

interpolate
^^^^^^^^^^^

.. java:method:: @Override public Instance interpolate(Instance ins)
   :outertype: ComplexPolygonInterpolator

interpolateAtTime
^^^^^^^^^^^^^^^^^

.. java:method:: public TGPair interpolateAtTime(TGPair tgp, TGPair nextTgp, long time)
   :outertype: ComplexPolygonInterpolator

interpolateTGPair
^^^^^^^^^^^^^^^^^

.. java:method:: @Override public TreeSet<TGPair> interpolateTGPair(TGPair tgp, TGPair nextTgp, EventType et)
   :outertype: ComplexPolygonInterpolator

isValidEventType
^^^^^^^^^^^^^^^^

.. java:method:: @Override public boolean isValidEventType(EventType e)
   :outertype: ComplexPolygonInterpolator

moveCoordinates
^^^^^^^^^^^^^^^

.. java:method:: protected Coordinate[] moveCoordinates(Geometry firstGeometry, Geometry nextGeometry, double factor)
   :outertype: ComplexPolygonInterpolator

moveCoordinatesBackward
^^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: protected Coordinate[] moveCoordinatesBackward(Geometry firstGeometry, Geometry nextGeometry, double factor)
   :outertype: ComplexPolygonInterpolator

   Move a coordinate array backwards in space by interpolating the centroid with respect to time and use the produced offset to move the geometry.

   :param firstGeometry:
   :param nextGeometry:
   :param factor:

shiftVerticesByOffset
^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public static Geometry shiftVerticesByOffset(Geometry geom, int offset)
   :outertype: ComplexPolygonInterpolator

validateGeometry
^^^^^^^^^^^^^^^^

.. java:method:: static Geometry validateGeometry(Geometry geom)
   :outertype: ComplexPolygonInterpolator

   Validates the given polygon-based geometry. Tries to simplify the geometry. Simplification is used for multipolygon elimination. This is much experimental, use it at your own risk.

   :param geom: -- polygon-based geometry

