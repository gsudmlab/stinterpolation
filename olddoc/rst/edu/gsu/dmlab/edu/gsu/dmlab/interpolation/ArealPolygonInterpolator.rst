.. java:import:: java.util ArrayList

.. java:import:: java.util TreeSet

.. java:import:: javax.swing.plaf.synth SynthSeparatorUI

.. java:import:: com.vividsolutions.jts.geom Coordinate

.. java:import:: com.vividsolutions.jts.geom Geometry

.. java:import:: com.vividsolutions.jts.geom MultiPolygon

.. java:import:: com.vividsolutions.jts.geom Polygon

.. java:import:: edu.gsu.dmlab.solgrind.base EventType

.. java:import:: edu.gsu.dmlab.solgrind.base Instance

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential TGPair

.. java:import:: edu.gsu.dmlab.solgrind.base.types.essential Trajectory

.. java:import:: edu.gsu.dmlab.util InterpolationConstants

.. java:import:: edu.gsu.dmlab.util PosisitonEstimator

ArealPolygonInterpolator
========================

.. java:package:: edu.gsu.dmlab.edu.gsu.dmlab.interpolation
   :noindex:

.. java:type:: public class ArealPolygonInterpolator extends ComplexPolygonInterpolator

Methods
-------
createInterpolatedGeometry
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. java:method:: public Geometry createInterpolatedGeometry(Geometry interpolatedGeometry, double desiredArea)
   :outertype: ArealPolygonInterpolator

   Create an interpolated geometry based on an initial geometry that will be buffered to reach a desired area. The buffer distance will be reduced once the area of the geometry approaches the desired area for better accuracy.

   :param interpolatedGeometry:
   :param desiredArea:

interpolate
^^^^^^^^^^^

.. java:method:: @Override public Instance interpolate(Instance ins)
   :outertype: ArealPolygonInterpolator

interpolateAtTime
^^^^^^^^^^^^^^^^^

.. java:method:: @Override public TGPair interpolateAtTime(TGPair tgp, TGPair nextTgp, long time)
   :outertype: ArealPolygonInterpolator

   Uses the areal interpolation to interpolated a segment trajectory of 2 tg-pairs.

   :param tgp: The first tg-pair to be used in the interpolation
   :param nextTgp: The second tg-pair to be used in the interpolation
   :param time: The time to be interpolated in

interpolateTGPair
^^^^^^^^^^^^^^^^^

.. java:method:: @Override public TreeSet<TGPair> interpolateTGPair(TGPair tgp, TGPair nextTgp, EventType et)
   :outertype: ArealPolygonInterpolator

isValidEventType
^^^^^^^^^^^^^^^^

.. java:method:: @Override public boolean isValidEventType(EventType e)
   :outertype: ArealPolygonInterpolator

