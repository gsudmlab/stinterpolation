.. java:import:: edu.gsu.dmlab.solgrind.base EventType

InterpolationConstants.DistanceMeasure
======================================

.. java:package:: edu.gsu.dmlab.util
   :noindex:

.. java:type:: public enum DistanceMeasure
   :outertype: InterpolationConstants

Enum Constants
--------------
DTW
^^^

.. java:field:: public static final InterpolationConstants.DistanceMeasure DTW
   :outertype: InterpolationConstants.DistanceMeasure

EUCLIDEAN
^^^^^^^^^

.. java:field:: public static final InterpolationConstants.DistanceMeasure EUCLIDEAN
   :outertype: InterpolationConstants.DistanceMeasure

