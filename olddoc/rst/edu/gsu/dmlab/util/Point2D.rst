Point2D
=======

.. java:package:: edu.gsu.dmlab.util
   :noindex:

.. java:type:: @SuppressWarnings public class Point2D extends java.awt.geom.Point2D.Double

   :author: Thaddeus Gholston, Data Mining Lab, Georgia State University

Constructors
------------
Point2D
^^^^^^^

.. java:constructor:: public Point2D()
   :outertype: Point2D

Point2D
^^^^^^^

.. java:constructor:: public Point2D(double x, double y)
   :outertype: Point2D

