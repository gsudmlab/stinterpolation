.. java:import:: com.vividsolutions.jts.geom Coordinate

.. java:import:: com.vividsolutions.jts.geom GeometryFactory

.. java:import:: com.vividsolutions.jts.geom Polygon

.. java:import:: edu.gsu.dmlab.util Point2D

.. java:import:: edu.gsu.dmlab.util CoordinateSystemConverter

PosisitonEstimator
==================

.. java:package:: edu.gsu.dmlab.util
   :noindex:

.. java:type:: public class PosisitonEstimator

Fields
------
THETA
^^^^^

.. java:field:: static final double THETA
   :outertype: PosisitonEstimator

Methods
-------
calcNewLoc
^^^^^^^^^^

.. java:method::  Point2D calcNewLoc(Point2D pointIn, double millisecs)
   :outertype: PosisitonEstimator

getPredictedPos
^^^^^^^^^^^^^^^

.. java:method:: public Point2D getPredictedPos(Point2D point, double millisecs)
   :outertype: PosisitonEstimator

getPredictedPos
^^^^^^^^^^^^^^^

.. java:method:: public Polygon getPredictedPos(Polygon poly, double millisecs)
   :outertype: PosisitonEstimator

