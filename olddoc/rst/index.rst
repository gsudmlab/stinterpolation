=======
STInterpolation
=======

This is the documentation of **STInterpolation**.

STInterpolation is a java project that provides 4 spatiotemporal interpolation techniques:
Complex Interpolation, Areal Interpolation, MBR Interpolation and Filament Interpolation.
To further enrish the spatiotemporal event trajectories, at the end of each interpolation,
the trajectory is further extrapolated for a period of time T that differs from one solar event to another.


The following chapters will explain the STInterpolation in detail:

.. toctree::
   :numbered:
   :maxdepth: 2
   :caption: Content:
   
   Introduction
   edu/gsu/dmlab/edu/gsu/dmlab/interpolation/package-index
   edu/gsu/dmlab/solgrind/package-index
   edu/gsu/dmlab/solgrind/base/package-index
   edu/gsu/dmlab/solgrind/base/operations/package-index
   edu/gsu/dmlab/solgrind/base/types/essential/package-index
   edu/gsu/dmlab/solgrind/base/types/event/package-index
   edu/gsu/dmlab/util/package-index
   Papers
   License
   Sponsors
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
