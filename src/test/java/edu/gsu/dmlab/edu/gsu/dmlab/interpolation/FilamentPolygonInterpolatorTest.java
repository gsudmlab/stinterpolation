package edu.gsu.dmlab.edu.gsu.dmlab.interpolation;

import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.TreeSet;

import org.junit.Test;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.WKTReader;

import interpolation.FilamentPolygonInterpolator;
import solgrind.base.EventType;
import solgrind.base.Instance;
import solgrind.base.types.essential.TGPair;
import solgrind.base.types.essential.TInterval;
import solgrind.base.types.essential.Trajectory;

public class FilamentPolygonInterpolatorTest {
	static FilamentPolygonInterpolator interpolator = new FilamentPolygonInterpolator();
		public static void main(String [] args) throws ParseException, com.vividsolutions.jts.io.ParseException{
		System.out.println("testinterpolateTGPair");
		testinterpolateTGPair();
		
		/*System.out.println("testinterpolate");
		testinterpolate();
		
		System.out.println("testinterpolateattime");
		testinterpolateattime();
		
		System.out.println("testcreateInterpolatedGeometry");
		testcreateInterpolatedGeometry();
		
		//String
	/* time = "2009-07-20 05-33"; SimpleDateFormat df = new
	 * SimpleDateFormat("yyyy-MM-dd hh-mm"); Date dt = df.parse(time);
	 *  Long l =
	 * dt.getTime(); //Equal or Greater than JDK 1.5 System.out.println(l);
	 * 
	 * time = "2009-07-20 05-40"; df = new SimpleDateFormat("yyyy-MM-dd hh-mm");
	 * dt = df.parse(time); l = dt.getTime(); //Equal or Greater than JDK 1.5
	 * System.out.println(l);*/
	 
	 }
	
	

	//@Test
	public static void testinterpolateTGPair() throws com.vividsolutions.jts.io.ParseException, ParseException {

		WKTReader reader = new WKTReader();
		Geometry geom1 = reader.read(
				"POLYGON((-932.1 -204.9,-931.5 -204.3,-892.5 -314.7,-873.3 -322.5,-874.5 -375.9,-924.3 -284.1,-932.1 -204.9))");
		Geometry geom2 = reader.read(
				"POLYGON((-916.5 -234.9,-915.9 -234.3,-904.5 -216.9,-912.9 -164.7,-885.3 -179.1,-888.3 -237.9,-855.9 -317.1,-833.1 -322.5,-830.7 -413.1,-858.9 -366.3,-873.3 -371.1,-890.1 -318.9,-904.5 -317.7,-916.5 -234.9))");

		TGPair tgp1 = new TGPair("2013-12-07 09:08:00", geom1);
		TGPair tgp2 = new TGPair("2013-12-07 09:14:00", geom2);

		TreeSet<TGPair> interpolatedSet = interpolator.interpolateTGPair(tgp1, tgp2, new EventType("ar"));
		
		Geometry interpolated = interpolatedSet.first().getGeometry();
		Geometry ans = reader.read(
				"POLYGON ((-932.1 -204.9, -931.5 -204.3, -892.5 -314.7, -873.3 -322.5, -874.5 -375.9, -924.3 -284.1, -932.1 -204.9))");
		System.out.println(interpolated.toString());
		//assertTrue(ans.equals(interpolated));

	}

	//@Test
	public static void testinterpolateattime() throws com.vividsolutions.jts.io.ParseException, ParseException {

		WKTReader reader = new WKTReader();
		Geometry geom1 = reader.read(
				"POLYGON((-932.1 -204.9,-931.5 -204.3,-892.5 -314.7,-873.3 -322.5,-874.5 -375.9,-924.3 -284.1,-932.1 -204.9))");
		Geometry geom2 = reader.read(
				"POLYGON((-916.5 -234.9,-915.9 -234.3,-904.5 -216.9,-912.9 -164.7,-885.3 -179.1,-888.3 -237.9,-855.9 -317.1,-833.1 -322.5,-830.7 -413.1,-858.9 -366.3,-873.3 -371.1,-890.1 -318.9,-904.5 -317.7,-916.5 -234.9))");

		TGPair tgp1 = new TGPair("2013-12-07 09:00:00", geom1);
		TGPair tgp2 = new TGPair("2013-12-07 09:14:00", geom2);

		TGPair interpolated = interpolator.interpolateAtTime(tgp1, tgp2, TInterval.convertTimestampToLong("2013-12-07 09:07:00"));
		
		Geometry interpolatedgeom = interpolated.getGeometry();
		Geometry ans = reader.read(
				"POLYGON ((-924.2565216915111 -237.4602899178475, -912.5165520506075 -218.4588422504218, -919.4726197098558 -174.41305517684842, -902.8482015112126 -183.0866646717922, -905.5826212712453 -240.14030838468523, -872.9263813646106 -320.1506994773402, -869.954202200077 -322.81125905774326, -850.4032327976483 -327.4545168277044, -848.5708307006752 -396.6276959884942, -868.5664789524592 -363.9304950232325, -872.4097609385143 -362.8883931378518, -882.9815887820932 -366.3539876995921, -899.0870805997337 -317.239791657316, -901.7961768187334 -315.527767003012, -913.0853713459313 -314.5412273023352, -924.2565216915111 -237.4602899178475))");
		assertTrue(ans.equals(interpolatedgeom));

	}	
	
	
//	@Test
	public static void testinterpolate() throws com.vividsolutions.jts.io.ParseException, ParseException {

		WKTReader reader = new WKTReader();
		Geometry geom1 = reader.read(
				"POLYGON((-932.1 -204.9,-931.5 -204.3,-892.5 -314.7,-873.3 -322.5,-874.5 -375.9,-924.3 -284.1,-932.1 -204.9))");
		Geometry geom2 = reader.read(
				"POLYGON((-916.5 -234.9,-915.9 -234.3,-904.5 -216.9,-912.9 -164.7,-885.3 -179.1,-888.3 -237.9,-855.9 -317.1,-833.1 -322.5,-830.7 -413.1,-858.9 -366.3,-873.3 -371.1,-890.1 -318.9,-904.5 -317.7,-916.5 -234.9))");

		TGPair tgp1 = new TGPair("2013-12-07 09:00:00", geom1);
		TGPair tgp2 = new TGPair("2013-12-07 09:10:00", geom2);

		Trajectory traj = new Trajectory();
		traj.addTGPair(tgp1);
		traj.addTGPair(tgp2);
		
		Instance ins = new Instance("123", new EventType(""), traj);
		Instance interpolatedins = interpolator.interpolate(ins);
		
		
		TreeSet<TGPair> interpolatedSet = interpolatedins.getTrajectory().getTGPairs();
		TreeSet<TGPair> ans = new TreeSet<TGPair>();
		ans.add(new TGPair(1386424800000L, reader.read("POLYGON ((-932.1 -204.9, -931.5 -204.3, -892.5 -314.7, -873.3 -322.5, -874.5 -375.9, -924.3 -284.1, -932.1 -204.9))")));
		ans.add(new TGPair(1386425160000L, reader.read("POLYGON ((-924.2565216915111 -237.4602899178475, -912.5165520506075 -218.4588422504218, -919.4726197098558 -174.41305517684842, -902.8482015112126 -183.0866646717922, -905.5826212712453 -240.14030838468523, -872.9263813646106 -320.1506994773402, -869.954202200077 -322.81125905774326, -850.4032327976483 -327.4545168277044, -848.5708307006752 -396.6276959884942, -868.5664789524592 -363.9304950232325, -872.4097609385143 -362.8883931378518, -882.9815887820932 -366.3539876995921, -899.0870805997337 -317.239791657316, -901.7961768187334 -315.527767003012, -913.0853713459313 -314.5412273023352, -924.2565216915111 -237.4602899178475))")));
		ans.add(new TGPair(1386425520000L, reader.read("POLYGON ((-916.5 -234.9, -915.9 -234.3, -904.5 -216.9, -912.9 -164.7, -885.3 -179.1, -888.3 -237.9, -855.9 -317.1, -833.1 -322.5, -830.7 -413.1, -858.9 -366.3, -873.3 -371.1, -890.1 -318.9, -904.5 -317.7, -916.5 -234.9))")));
		ans.add(new TGPair(1386425880000L, reader.read("POLYGON ((-916.3426325504265 -234.90101858722815, -904.2518330990471 -216.90099657034648, -912.6410722924453 -164.70076593702302, -884.9716857958457 -179.1007769519955, -888.0671971736112 -237.90086735046748, -855.642719204578 -317.1011390212293, -832.8071859137755 -322.5010853413222, -830.4915030564114 -413.1014067741925, -858.6863529718466 -366.30146443877453, -873.1769873579486 -371.10133843401553, -889.9518995486493 -318.9012348331848, -904.4633213866887 -317.7011598756341, -916.3426325504265 -234.90101858722815))")));
		
		for (TGPair tgp : interpolatedSet){
			System.out.print("ans.add(new TGPair("+tgp.getTInterval().getStartTime()+"L, reader.read(\""+tgp.getGeometry()+"\")));");
			System.out.println();
		}
		//assertTrue(interpolatedSet.equals(ans));
		}


}
//