/*////////////////////////////////////////////////////////////////////////
ComplexPolygonInterpolator.java

Copyright (C) 2017  Soukaina Filali Boubrahimi, Berkay Aydin, Dustin Kempton, Rafal Angryk
  Data Mining Laboratory at Georiga State University 
  Principle author contact: sfilaliboubrahimi1@cs.gsu.edu

This file is part of SpatioTemporalInterpolation*.

SpatioTemporalInterpolation* is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; only version 2
of the License. See the COPYING file for more information.
////////////////////////////////////////////////////////////////////*/
package interpolation;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.TopologyException;
import com.vividsolutions.jts.simplify.DouglasPeuckerSimplifier;

import net.sf.javaml.distance.fastdtw.dtw.FastDTW;
import net.sf.javaml.distance.fastdtw.dtw.TimeWarpInfo;
import net.sf.javaml.distance.fastdtw.matrix.ColMajorCell;
import net.sf.javaml.distance.fastdtw.timeseries.TimeSeries;
import net.sf.javaml.distance.fastdtw.timeseries.TimeSeriesPoint;
import solgrind.base.EventType;
import solgrind.base.Instance;
import solgrind.base.types.essential.TGPair;
import solgrind.base.types.essential.Trajectory;
import util.InterpolationConstants;
import util.PosisitonEstimator;

public class ComplexPolygonInterpolator extends Interpolator{

	@Override
	public Instance interpolate(Instance ins) {
		System.out.println("\t\t\t\t\tInterpolating instance " + ins.getId() );
		Trajectory iTraj = new Trajectory();
		if(! isValidEventType(ins.getType()) ){
			return null;
		} 
	//	System.out.println("\tIns::: \n" + ins);
		//interpolate here
		Instance adjusted = Interpolator.adjustTimestamps(ins);
//		System.out.println("\t\tAdjusted::: \n" + adjusted);
		ArrayList<TGPair> tgpList = new ArrayList<TGPair>(adjusted.getTrajectory().getTGPairs());
		for(int i = 0; i < tgpList.size(); i++){
			
			TGPair tgp = tgpList.get(i);
		//	System.out.println("Interpolating... " + tgp);
			TGPair nextTgp = null;
			if (i != tgpList.size()-1){
				nextTgp = tgpList.get(i+1);
			} 
	//		System.out.println("Interpolating... InstanceID: " + ins.getId() + " Time: " + tgp.getTInterval());
			TreeSet<TGPair> interpolated = interpolateTGPair(tgp, nextTgp, ins.getType());
			cleanEmptyPolygons(interpolated);
			
			iTraj.getTGPairs().addAll(interpolated);
		}
		iTraj.addTGPair(tgpList.get(tgpList.size()-1));
		Instance iIns = new Instance(ins.getId(), ins.getType(), iTraj);
	//	System.out.println("\t\tInterpolated::: \n" + iIns);
		return iIns;

	}



	static TreeSet<TGPair> cleanEmptyPolygons(TreeSet<TGPair> interpolated) {
		ArrayList<TGPair> tgpList = new ArrayList<TGPair>(interpolated);
		if(tgpList.get(0).getGeometry().isEmpty()){
			System.out.println("ERROR! First polygon must not be an empty polygon!");
		}
		for(int i = 1; i < tgpList.size(); i++){
			
			if(tgpList.get(i).getGeometry().isEmpty()){
				//System.out.println("FIXING EMPTY GEOMETRY");
				tgpList.get(i).setGeometry( tgpList.get(i-1).getGeometry() );
			}
		}
		
		return new TreeSet<TGPair>(tgpList);
	}


	@Override
	public TreeSet<TGPair> interpolateTGPair(TGPair tgp, TGPair nextTgp, EventType et) {
		TreeSet<TGPair> interpolated = new TreeSet<TGPair>();
		TGPair tgp_i_start = new TGPair(tgp.getTInterval().getStartTime(),
						tgp.getTInterval().getStartTime() + InterpolationConstants.I_INTERVAL,
						tgp.getGeometry(),tgp.getKBarchivID(),false);
		//System.out.println("start is "+tgp.getKBarchivID());
		System.out.println("KB Arhiv id is "+ tgp.getKBarchivID() );
		interpolated.add(tgp_i_start);//add original to the beginning
		if(nextTgp == null){
			PosisitonEstimator pe= new PosisitonEstimator();
			/*Geometry geom= pe.getPredictedPos(p,0)
			nextTgp = new TGPair(tgp.getTInterval().getEndTime(),
								tgp.getTInterval().getEndTime() + 1, 
								tgp.getGeometry(),tgp.getKBarchivID(),false);*/
			//start interpolating
			long ts = tgp.getTInterval().getStartTime();
			long te = tgp.getTInterval().getEndTime() + InterpolationConstants.getEventPropagation(et);
			for(long i = ts + InterpolationConstants.I_INTERVAL; i < te; i += InterpolationConstants.I_INTERVAL){
				TGPair tgpi = createInterpolatedTGPair(i, i+InterpolationConstants.I_INTERVAL, pe.getPredictedPos((Polygon)tgp.getGeometry(),(i - ts)).getCoordinates());
				interpolated.add(tgpi);
			} //end of interpolation	
			return interpolated;
		}
			Geometry densifiedGeometry = (Polygon) tgp.getGeometry();
			Geometry densifiedNextGeom = (Polygon) nextTgp.getGeometry();
		if(tgp.getGeometry().getNumPoints()>10 && nextTgp.getGeometry().getNumPoints()>10){
			 densifiedGeometry = Interpolator.densify((Polygon) tgp.getGeometry());
			 densifiedNextGeom = Interpolator.densify((Polygon) nextTgp.getGeometry());
		}
		
		TimeSeries tgpTS= convertToTimeseries(densifiedGeometry);
		HashMap<Integer, TimeSeries> offsetTimeSeriesMap = createOffsetTimeSeriesMap(densifiedNextGeom);

		
		boolean arealValid = false;
		int K = 0;
		do{
			//get a similar warp path
			K++;
			Entry<Integer, TimeWarpInfo> offsetWarpInfoPair = getKthMostSimilarWarpPathInfo(tgpTS, offsetTimeSeriesMap, K);
			int offset = offsetWarpInfoPair.getKey();
			TimeWarpInfo warpInfo = offsetWarpInfoPair.getValue();
			
			//create a shifted geometry
			densifiedNextGeom = shiftVerticesByOffset(densifiedNextGeom, offset);
			
			
			//start interpolating
			long ts = tgp.getTInterval().getStartTime();
			long te = nextTgp.getTInterval().getStartTime();
			for(long i = ts + InterpolationConstants.I_INTERVAL; i < te; i += InterpolationConstants.I_INTERVAL){
				double factor = (double)(i - ts) / (double)(te - ts);
				Coordinate[] i_coordinates = createInterpolatedCoordinates(densifiedGeometry,
										densifiedNextGeom, warpInfo, factor);
				TGPair tgpi = createInterpolatedTGPair(i, i+InterpolationConstants.I_INTERVAL, i_coordinates);
				interpolated.add(tgpi);
			} //end of interpolation
			
			//check areal validity
			arealValid = arealValidation(interpolated, tgp, nextTgp);
			if(!arealValid){
				//System.out.println("K is " + K);
				if(K >= InterpolationConstants.TIMESERIES_SIMILARITY_STEPS/3){break;}
				//printTGPairGeometries(interpolated);
			}
			
		} while(!arealValid);
		
		arealValid = arealValidation(interpolated, tgp, nextTgp);
		if(!arealValid){
			//arealValid = arealValidation2(interpolated, tgp, nextTgp);
			arealInvalid_count++;
			//System.out.println("Calling naive interpolate");
			//interpolated = naiveInterpolate(tgp, nextTgp);
			ArealPolygonInterpolator api=new ArealPolygonInterpolator();
			interpolated=api.interpolateTGPair(tgp, nextTgp, et);
			//printTGPairGeometries(interpolated);
		}
		return interpolated;
	}
	
	public TGPair interpolateAtTime(TGPair tgp, TGPair nextTgp, long time) {
	//	System.out.println("COMPLEX AT TIME");
		TreeSet<TGPair> interpolated= new TreeSet<TGPair>();
		if(nextTgp == null){
			return null;	
			}
		Geometry densifiedGeometry = (Polygon) tgp.getGeometry();
		Geometry densifiedNextGeom = (Polygon) nextTgp.getGeometry();
		
		if(tgp.getGeometry().getNumPoints()>10 && nextTgp.getGeometry().getNumPoints()>10){
			 densifiedGeometry = validateGeometry(Interpolator.densify((Polygon) tgp.getGeometry()));
			 densifiedNextGeom = validateGeometry(Interpolator.densify((Polygon) nextTgp.getGeometry()));
		}
		TimeSeries tgpTS= convertToTimeseries(densifiedGeometry);
		HashMap<Integer, TimeSeries> offsetTimeSeriesMap = createOffsetTimeSeriesMap(densifiedNextGeom);
		boolean arealValid = false;
		int K = 0;
		do{
			K++;
			Entry<Integer, TimeWarpInfo> offsetWarpInfoPair = getKthMostSimilarWarpPathInfo(tgpTS, offsetTimeSeriesMap, K);
			int offset = offsetWarpInfoPair.getKey();
			TimeWarpInfo warpInfo = offsetWarpInfoPair.getValue();
			densifiedNextGeom = shiftVerticesByOffset(densifiedNextGeom, offset);
			
			//start interpolating
			long ts = tgp.getTInterval().getStartTime();
			long te = nextTgp.getTInterval().getStartTime();
		
			double factor = (double)(time - ts) / (double)(te - ts);
			Coordinate[] i_coordinates = createInterpolatedCoordinates(densifiedGeometry,
										densifiedNextGeom, warpInfo, factor);
			TGPair tgpi = createInterpolatedTGPair(time, time+InterpolationConstants.I_INTERVAL, i_coordinates);
			interpolated.add(tgpi);
			//check areal validity
			arealValid = arealValidation(interpolated, tgp, nextTgp);
			if(!arealValid){
				//System.out.println("K is " + K);
				if(K >= InterpolationConstants.TIMESERIES_SIMILARITY_STEPS/3){break;}
				//printTGPairGeometries(interpolated);
			}
			
		} while(!arealValid);
		
		arealValid = arealValidation(interpolated, tgp, nextTgp);
		if(!arealValid){
			ComplexPolygonInterpolator.arealInvalid_count++;
			ArealPolygonInterpolator api=new ArealPolygonInterpolator();
			return api.interpolateAtTime(tgp, nextTgp, time);
		}
		return interpolated.first();
	}
	
	private TreeSet<TGPair> naiveInterpolate(TGPair tgp, TGPair nextTgp) {
		TreeSet<TGPair> interpolated = new TreeSet<TGPair>();
		TGPair tgp_i_start = new TGPair(tgp.getTInterval().getStartTime(),
						tgp.getTInterval().getStartTime() + InterpolationConstants.I_INTERVAL,
						tgp.getGeometry());
		interpolated.add(tgp_i_start);
		
		if(nextTgp == null){
			nextTgp = new TGPair(tgp.getTInterval().getEndTime(),
								tgp.getTInterval().getEndTime() + 1, 
								tgp.getGeometry());
		}
		
		Geometry firstGeometry = tgp.getGeometry();
		Geometry nextGeometry = nextTgp.getGeometry();
		
		
		//start interpolating
		long ts = tgp.getTInterval().getStartTime();
		long te = nextTgp.getTInterval().getStartTime();
		for(long i = ts + InterpolationConstants.I_INTERVAL; i < te; i += InterpolationConstants.I_INTERVAL){
			double factor = (double)(i - ts) / (double)(te - ts);
			Coordinate[] i_coordinates = moveCoordinates(firstGeometry, nextGeometry, factor);
			TGPair tgpi = createInterpolatedTGPair(i, i+InterpolationConstants.I_INTERVAL, i_coordinates);
			interpolated.add(tgpi);
		} //end of interpolation
		
		return interpolated;
	}



	protected Coordinate[] moveCoordinates(Geometry firstGeometry, Geometry nextGeometry, double factor) {
		Point c_f = firstGeometry.getCentroid();
		Point c_n = nextGeometry.getCentroid();
		Coordinate c_i = new Coordinate(linearInterpolate(c_f.getX(), c_n.getX(), factor), 
						linearInterpolate(c_f.getY(), c_n.getY(), factor));
		double offsetX = c_i.x - c_f.getX();
		double offsetY = c_i.y - c_f.getY();
		Coordinate i_coordinates[] = new Coordinate[firstGeometry.getCoordinates().length];
		int index = 0;
		for(Coordinate c : firstGeometry.getCoordinates()){
			Coordinate c_i_j = new Coordinate(c.x + offsetX, c.y + offsetY);
			i_coordinates[index] = c_i_j;
			index++;
		}
		return i_coordinates;
	}
	/**
	 * Move a coordinate array backwards in space by interpolating the centroid 
	 * with respect to time and use the produced offset to move the geometry.
	 * @param firstGeometry
	 * @param nextGeometry
	 * @param factor
	 * @return
	 */
	protected Coordinate[] moveCoordinatesBackward(Geometry firstGeometry, Geometry nextGeometry, double factor) {
		Point c_f = firstGeometry.getCentroid();
		Point c_n = nextGeometry.getCentroid();
		
		try{Coordinate c_i = new Coordinate(linearInterpolate(c_f.getX(), c_n.getX(), factor), 
						linearInterpolate(c_f.getY(), c_n.getY(), factor));
		double offsetX = c_i.x - c_f.getX();
		double offsetY = c_i.y - c_f.getY();
		Coordinate i_coordinates[] = new Coordinate[nextGeometry.getCoordinates().length];
		int index = 0;
		for(Coordinate c : nextGeometry.getCoordinates()){
			Coordinate c_i_j = new Coordinate(c.x - offsetX, c.y - offsetY);
			i_coordinates[index] = c_i_j;
			index++;
		}
		return i_coordinates;
		}
		catch(Exception e){
			System.err.println("The first point is "+c_f.toString()+" The second point is "+c_n.toString());
			System.err.println("The first geom is "+firstGeometry.toString()+"\n\n The second point is "+nextGeometry.toString());
			System.exit(1);
		};
		return null;
	}


	public static Entry<Integer, TimeWarpInfo> getKthMostSimilarWarpPathInfo(TimeSeries tgpTS,
			HashMap<Integer, TimeSeries> offsetTimeSeriesMap, int k) {

		double score = Double.MAX_VALUE;
		TreeMap<Double, Integer> scoreOffsetMap = new TreeMap<Double, Integer>();
		TreeMap<Integer, TimeWarpInfo> offsetWarpInfoMap = new TreeMap<Integer, TimeWarpInfo>();
		Map.Entry<Integer,TimeWarpInfo> distWarpInfoEntry = null;
				
		for( Entry<Integer, TimeSeries> kv: offsetTimeSeriesMap.entrySet() ){
			TimeWarpInfo warpInfo = getWarpInfo(tgpTS, kv.getValue() );
			//System.out.println("Warp Path:     " + warpInfo.getPath()); //DEBUG-ONLY
			double tempScore = warpInfo.getDistance();
			scoreOffsetMap.put(tempScore, kv.getKey());
			offsetWarpInfoMap.put(kv.getKey(), warpInfo);
			
			if(tempScore < score){ //finding the minimum DTW distance
				score = tempScore;
				distWarpInfoEntry = new AbstractMap.SimpleEntry<Integer,TimeWarpInfo>(kv.getKey(), warpInfo);
			}
		}
		
		ArrayList<Double> scoreList = new ArrayList<Double>(scoreOffsetMap.keySet());
		//get Kth smallest score (k starts from 1, so decrease 1) 
		double kth_score;
		if(k==-9999)  kth_score = scoreList.get( InterpolationConstants.TIMESERIES_SIMILARITY_STEPS % scoreList.size() );// sentinel value
		else  kth_score = scoreList.get( (k-1) % scoreList.size() );
		int kth_offset = scoreOffsetMap.get(kth_score);
		TimeWarpInfo kth_warpInfo = offsetWarpInfoMap.get(kth_offset);
		distWarpInfoEntry = new AbstractMap.SimpleEntry<Integer,TimeWarpInfo>(kth_offset, kth_warpInfo);
		
		return distWarpInfoEntry;
		
		//return null;
	}
	
	public static int arealInvalid_count = 0;
	protected boolean arealValidation(TreeSet<TGPair> interpolated, TGPair tgp, TGPair nextTgp) {
		
		double initialArea = tgp.getGeometry().getArea();
		double lastArea = nextTgp.getGeometry().getArea();
		
		double minArea = Math.min(initialArea, lastArea);
		double error = 0.30;
		minArea *= (1.0 - error);
		
		double interpolatedMinimum = Double.MAX_VALUE;
		for(TGPair itgp : interpolated){
			double iArea = itgp.getGeometry().getArea();
			if(iArea < interpolatedMinimum){
				interpolatedMinimum = iArea;
			}
		}
		double ratio = interpolatedMinimum / minArea;
		if(ratio < ( 1.0 - error )){
			//System.out.println("Areal invalidation. Factor:" + ratio);
			return false;
		}

		return true;
	}
	private boolean arealValidation2(TreeSet<TGPair> interpolated, TGPair tgp, TGPair nextTgp) {
		
		double initialArea = tgp.getGeometry().getArea();
		double lastArea = nextTgp.getGeometry().getArea();
		
		double minArea = Math.min(initialArea, lastArea);
		double error = 0.30;
		minArea = minArea * (1.0 - error);
		
		double interpolatedMinimum = Double.MAX_VALUE;
		for(TGPair itgp : interpolated){
			double iArea = itgp.getGeometry().getArea();
			if(iArea < interpolatedMinimum){
				interpolatedMinimum = iArea;
			}
		}
		double ratio = interpolatedMinimum / minArea;
		if(ratio < ( 1.0 - error )){
			System.out.println("Areal invalidation. Factor:" + ratio);
			return false;
		}

		return true;
		
//		for(TGPair itgp : interpolated){
//			double iArea = itgp.getGeometry().getArea();
//			if(iArea < minArea){
//				
//				return false;
//			}
//		}
		
	}

	private void printTGPairGeometries(TreeSet<TGPair> tgpList){
		//String geoms = "";
		for(TGPair itgp : tgpList){
			//geoms.concat(itgp.getGeometry().toString() );
			//geoms.concat(itgp.getGeometry()+"\n");
			System.out.println(itgp.getGeometry());
		}
		System.out.println("End of tgpList");
	}


	static Coordinate[] createInterpolatedCoordinates(Geometry densifiedGeom, Geometry densifiedNextGeom,
			TimeWarpInfo warpInfo, double factor) {
		Coordinate[] i_coordinates = new Coordinate[warpInfo.getPath().size()+1];
		Coordinate[] cI_geom = densifiedGeom.getCoordinates();
		Coordinate[] cJ_geom = densifiedNextGeom.getCoordinates();
		
		//System.out.println("cI - size -- " + cI_geom.length + "\t cJ - size -- " + cJ_geom.length);
		//System.out.println("Warp Path:" + warpInfo.getPath());
		for(int i = 0; i < warpInfo.getPath().size(); i++){
			ColMajorCell cmc = warpInfo.getPath().get(i);
			int colIndex = cmc.getCol();
			int rowIndex = cmc.getRow();
			//System.out.println(i+" -- ColMajorCell:" + cmc.getCol() + "->" + cmc.getRow());
			
			Coordinate c1 = cI_geom[colIndex];
			Coordinate c2 = cJ_geom[rowIndex];
			i_coordinates[i] = interpolateCoordinate(c1, c2, factor);
		}
		//make it a closed ring
		i_coordinates[i_coordinates.length-1] = i_coordinates[0];
		return i_coordinates;
	}

	private static Coordinate interpolateCoordinate(Coordinate c1, Coordinate c2, double factor) {
		double c_i_x = linearInterpolate(c1.x, c2.x, factor);
		double c_i_y = linearInterpolate(c1.y, c2.y, factor);
		return new Coordinate(c_i_x, c_i_y);
	}
	
	/**
	 * Linearly interpolates a 1D variable (between x_0 and x_k). 
	 * factor is the step size
	 * Say you have x_0 = 1, x_k = 11, factor=0.4, your interpolated result will be 5
	 * @param x_0 - minimum bound
	 * @param x_k - maximum bound
	 * @param factor - the factor of linear interpolation (factor must be between 0 and 1)
	 * @return - interpolated result
	 */
	private static double linearInterpolate(double x_0, double x_k, double factor) {
		if(factor <0.0 || factor > 1.0){
			return Double.NaN;
		} else{
			return x_0 + ((x_k - x_0) * factor );
		}
		
	}

	public static int empty_count = 0;
	
	static TGPair createInterpolatedTGPair(long startTime, long endTime, Coordinate[] i_coordinates) {
		Geometry geom = new GeometryFactory().createPolygon(i_coordinates);
		Geometry sgeom = simplifyGeometry(geom, InterpolationConstants.SIMPLIFIER_DISTANCE_TOLERANCE);
		
		if (sgeom!=null){
			geom = validateGeometry(sgeom);
		}
		else if(sgeom==null && geom!=null){
			geom = validateGeometry(geom);
		}
		else if (sgeom==null && geom==null){
			empty_count++;
		}
		
		
	/*	if(geom.isEmpty()){
			//System.out.println("ERROR: The geometry is empty --" + geom);
			empty_count++;
		}*/
		
		return new TGPair(startTime, endTime, geom,null,true);
	}


	/**
	 * Validates the given polygon-based geometry. Tries to simplify the geometry.
	 * Simplification is used for multipolygon elimination. 
	 * This is much experimental, use it at your own risk.
	 * @param geom -- polygon-based geometry
	 * @return
	 */
	static Geometry validateGeometry(Geometry geom) {
		if(!geom.isValid()){
			Geometry geom_bv = GeometryValidator.bufferValidate(geom);
			if(!geom_bv.isValid()){
				Geometry geom_sv = GeometryValidator.simplifierValidate(geom);
				if(!geom_sv.isValid()){
					Geometry geom_pv = GeometryValidator.polygonizerValidate(geom);
					if(!geom_pv.isValid()){
						System.out.println("WARNING! Geometry validation has failed!!!");
					} else{
						geom = geom_pv;
					}
				} else{
					geom = geom_sv;
				}
			} else{
				geom = geom_bv;
			}
		}
		//simplify the geometry if it is a multipolygon 
		//(for 30 times)
		//with increasing tolerance for simplifier
		int i = 0;
		while(geom instanceof MultiPolygon){
			geom = geom.convexHull().buffer(0.1);
			//geom = simplifyGeometry(geom, InterpolationConstants.SIMPLIFIER_DISTANCE_TOLERANCE + i*0.1);
			i++;
			if(i > 30){break;}
		}
		// if 30 iterative simplification steps doesn't work
		// get the polygon with the largest area
		if(geom instanceof GeometryCollection){
			double maxArea = -1.0;
			for(i = 0; i < geom.getNumGeometries(); i++){
				if(maxArea < geom.getGeometryN(i).getArea() && geom.getGeometryN(i) instanceof Polygon){
					geom = geom.getGeometryN(i); //get the polygon with maximum area
				}
			}
		}
		if(((Polygon) geom).getNumInteriorRing()>0) geom=geom.convexHull();
		return geom;
	}

	private static Geometry simplifyGeometry(Geometry geom, double tolerance) {
		double originalArea = geom.getArea(); 
		DouglasPeuckerSimplifier dps = new DouglasPeuckerSimplifier(geom);
		dps.setEnsureValid(true);
		dps.setDistanceTolerance(tolerance);
		
		double simplifiedArea = dps.getResultGeometry().getArea();
		if(simplifiedArea / originalArea < 0.8){
//			System.out.println("\tRatio after simplification :  " + simplifiedArea / originalArea);
//			System.out.println("Geometry type: " + geom.getGeometryType());
//			System.out.println("Is Geometry valid?: " + new IsValidOp(geom).getValidationError());
//			System.out.println("Original geom: " + geom);
//			System.out.println("Simplified geom: " + dps.getResultGeometry());
//			System.out.println("Polygonized validate: " + GeometryValidator.polygonizerValidate(geom));
			Geometry geom_v = null;
			try{
				geom_v = GeometryValidator.polygonizerValidate(geom);
			} catch(TopologyException te){
				//System.out.println(geom);
				//te.printStackTrace();
				geom_v = geom.convexHull();
				//System.out.println(geom_v);
			} catch(IllegalArgumentException iae){
				geom_v = geom.convexHull();
			}
			
			
			if(geom_v instanceof MultiPolygon){
				double maxMPArea = geom_v.getGeometryN(0).getArea();
				Geometry geom_result = geom_v.getGeometryN(0);
				for(int i = 1; i < geom_v.getNumGeometries() ; i++){
					Geometry geom_v_i = geom_v.getGeometryN(i);
					if(geom_v_i.getArea() >= maxMPArea){
						maxMPArea = geom_v_i.getArea();
						geom_result = geom_v_i;
					}
				}
				return geom_result;
				//double simplifiedArea1 = geom_v.getArea();
				//double simplifiedArea2 = geom_result.getArea();
				//System.out.println("\t\tSA1::: Ratio after simplification :  " + simplifiedArea1 / originalArea);
				//System.out.println("\t\t\t\tRA1::: Ratio after simplification :  " + simplifiedArea2 / originalArea);
				
			} else{
				return geom_v;
				//System.out.println("THERE IS ALSO ONLY POLYGONS. THIS IS WEIRD HERE");
				//double simplifiedArea1 = geom_v.getArea();
				//System.out.println("\t\t\t\tRA2::: Ratio after simplification :  " + simplifiedArea1 / originalArea);
			}
			
		}
		return dps.getResultGeometry();
	}


	/**
	 * @param densifiedNextGeom
	 * @return
	 */
	public static HashMap<Integer, TimeSeries> createOffsetTimeSeriesMap(Geometry densifiedNextGeom) {
		HashMap<Integer, TimeSeries> offsetTimeSeriesMap = new HashMap<Integer, TimeSeries>();  
		int coordinateCount = densifiedNextGeom.getNumPoints();
		for(int i = 0; i < InterpolationConstants.TIMESERIES_SIMILARITY_STEPS; i++){
			int offset = (int)(i * ((double)coordinateCount / (double)InterpolationConstants.TIMESERIES_SIMILARITY_STEPS));
			TimeSeries ts = convertToTimeseriesByOffset(validateGeometry(densifiedNextGeom), offset);
			offsetTimeSeriesMap.put(offset, ts);
		}
		return offsetTimeSeriesMap;
	}

	
	
	public static Geometry shiftVerticesByOffset(Geometry geom, int offset) {
		if(geom == null){
			System.out.println("ERROR: Geometry is null, cannot create time series");
			return null;
		} else if(!(geom instanceof Polygon)){
			System.out.println("ERROR: Geometry is not a polygon, cannot create time series");
			System.out.println("Geometry is a " + geom.getGeometryType() + "--"+ geom );
			geom = validateGeometry(geom);
			return shiftVerticesByOffset(geom, offset);
		} else{
			Coordinate[] coordinates = geom.getCoordinates();
			//System.out.println(Arrays.asList(coordinates));
			Coordinate[] shiftedCoordinates = new Coordinate[coordinates.length+1];
			for(int i = offset; i <= coordinates.length+offset; i++){
				Coordinate c = coordinates[i % coordinates.length]; 
				shiftedCoordinates[i-offset]= c;
				//System.out.println(i-offset);
			}
			//System.out.println(Arrays.asList(shiftedCoordinates));
			return (Polygon) new GeometryFactory().createPolygon(shiftedCoordinates);
		}
	}

	@SuppressWarnings("unused")
	private Map.Entry<Integer, TimeWarpInfo> getMostSimilarWarpPathInfo(TimeSeries tgpTS, 
			HashMap<Integer, TimeSeries> offsetTimeSeriesMap) {
		double score = Double.MAX_VALUE;
		TreeMap<Double, Integer> scoreOffsetMap = new TreeMap<Double, Integer>();
		TreeMap<Integer, TimeWarpInfo> offsetWarpInfoMap = new TreeMap<Integer, TimeWarpInfo>();
		
		
		Map.Entry<Integer,TimeWarpInfo> distWarpInfoEntry = null;
				
		for( Entry<Integer, TimeSeries> kv: offsetTimeSeriesMap.entrySet() ){
			TimeWarpInfo warpInfo = getWarpInfo(tgpTS, kv.getValue() );
			//System.out.println("Warp Path:     " + warpInfo.getPath()); //DEBUG-ONLY
			double tempScore = warpInfo.getDistance();
			scoreOffsetMap.put(tempScore, kv.getKey());
			offsetWarpInfoMap.put(kv.getKey(), warpInfo);
			
			if(tempScore < score){ //finding the minimum DTW distance
				score = tempScore;
				distWarpInfoEntry = new 
						AbstractMap.SimpleEntry<Integer,TimeWarpInfo>(kv.getKey(), warpInfo);
			}
		}
		//System.out.print("Am I returning the min score? ");
		//System.out.println("\nReturned Score: " + score + "\n All Scores: " + scoreOffsetMap);
		
	//	System.out.println("Returned warp info::" + distWarpInfoEntry.getValue() );
	//	System.out.println("In Map warp info::" + offsetWarpInfoMap.get(scoreOffsetMap.firstEntry().getValue()) );
		
		
		return distWarpInfoEntry;
	}

	public static TimeWarpInfo getWarpInfo(TimeSeries tsI, TimeSeries tsJ) {
		return FastDTW.getWarpInfoBetween(tsI, tsJ, 0);
	}

	public static TimeSeries convertToTimeseries(Geometry geom) {
		if(geom == null){
			System.out.println("ERROR: Geometry is null, cannot create time series");
			return null;
		} else if(!(geom instanceof Polygon)){
			System.out.println("ERROR: Geometry is not a polygon, cannot create time series");
			System.out.println("Geometry is a " + geom.getGeometryType() + "--"+ geom );
			geom = validateGeometry(geom);
			return convertToTimeseries(geom);
		} else{
			//System.out.println(geom.getNumPoints());
			TimeSeries TSRepresentation = new TimeSeries(1); //1-dimensional time series
			Coordinate centroid = geom.getCentroid().getCoordinate();
			Coordinate[] geomCoordinates = geom.getCoordinates();
			for(int i = 0; i < geom.getCoordinates().length-1; i++){
				Coordinate c = geomCoordinates[i];
				final double distance = c.distance(centroid);
				TSRepresentation.addLast((int)i, new TimeSeriesPoint(new double[] {distance}));
			}
			//System.out.println("Size of TS:" + TSRepresentation.size());
			return TSRepresentation;
		}
	}


	private static TimeSeries convertToTimeseriesByOffset(Geometry geom, int offset) {
		
		if(geom == null){
			System.out.println("ERROR: Geometry is null, cannot create time series");
			return null;
		} else if(!(geom instanceof Polygon)){
			System.out.println("ERROR: Geometry is not a polygon, cannot create time series");
			System.out.println("Geometry is a " + geom.getGeometryType() + "--"+ geom );
			geom = validateGeometry(geom);
			return convertToTimeseries(geom);
		} else{
			//System.out.println(geom.getNumPoints());
			TimeSeries TSRepresentation = new TimeSeries(1); //1-dimensional time series
			Coordinate centroid = geom.getCentroid().getCoordinate();
			Coordinate[] coordinates = geom.getCoordinates();
			for(int i = offset; i < coordinates.length+offset; i++){
				Coordinate c = coordinates[i % coordinates.length ];
				final double distance = c.distance(centroid);
				TSRepresentation.addLast((int)i, new TimeSeriesPoint(new double[] {distance}));
			}
			//System.out.println("Size of TS:" + TSRepresentation.size());
			return TSRepresentation;
		}
	}

	@Override
	public boolean isValidEventType(EventType e) {
		return e.getType().equalsIgnoreCase("AR") 
				|| e.getType().equalsIgnoreCase("SS") 
				|| e.getType().equalsIgnoreCase("CH")
				|| e.getType().equalsIgnoreCase("FI");
	}
	
	/*private TimeSeries convertToDoubleTimeseries(Geometry geom) {
		if(geom == null){
			System.out.println("ERROR: Geometry is null, cannot create time series");
			return null;
		} else if(!(geom instanceof Polygon)){
			System.out.println("ERROR: Geometry is not a polygon, cannot create time series");
			return null;
		} else{
			//System.out.println(geom.getNumPoints());
			TimeSeries TSRepresentation = new TimeSeries(1); //1-dimensional time series
			Coordinate centroid = geom.getCentroid().getCoordinate();
			Coordinate[] coordinates = geom.getCoordinates();
			for(int i = 0; i < 2*coordinates.length-1; i++){
				Coordinate c = coordinates[i % coordinates.length ];
				final double distance = c.distance(centroid);
				TSRepresentation.addLast((int)i, new TimeSeriesPoint(new double[] {distance}));
			}
			//System.out.println("Size of TS:" + TSRepresentation.size());
			return TSRepresentation;
		}
	}*/
	
	/*@SuppressWarnings("unused")
	private int getMostSimilarTSCoordinateIndex(TimeSeries tgpTS, HashMap<Integer, TimeSeries> offsetTimeSeriesMap) {
		double score = Double.MAX_VALUE;
		int maxCoordinateIndex = -1;
		for( Entry<Integer, TimeSeries> kv: offsetTimeSeriesMap.entrySet() ){
			double tempScore = similarityScore( tgpTS, kv.getValue() );
			if(tempScore < score){
				score = tempScore;
				maxCoordinateIndex = kv.getKey();
			}
		}
		return maxCoordinateIndex;
	}*/
	
	/*private double similarityScore(TimeSeries tsI, TimeSeries tsJ) {
		double distance = Double.MAX_VALUE;
		if(InterpolationConstants.TS_SCORE_TYPE == DistanceMeasure.DTW){
			
			distance = FastDTW.getWarpDistBetween(tsI, tsJ);
			
			final TimeWarpInfo info = FastDTW.getWarpInfoBetween(tsI, tsJ, 0);
			
	        //System.out.println("Warp Distance: " + info.getDistance()); // DEBUG-ONLY
	        //System.out.println("Warp Path:     " + info.getPath()); //DEBUG-ONLY
	        return info.getDistance();
			
			
		} else if(InterpolationConstants.TS_SCORE_TYPE == DistanceMeasure.EUCLIDEAN){
			System.out.println("ERROR: Euclidean distance is not yet implemented here");
			//double dist = new EuclideanDistance().calculateDistance(ts1, ts2);
		}
		
		return distance;
	}*/
}
