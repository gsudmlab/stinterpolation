/*////////////////////////////////////////////////////////////////////////
FilamentInterpolator.java

Copyright (C) 2017  Soukaina Filali Boubrahimi, Berkay Aydin, Dustin Kempton, Rafal Angryk
  Data Mining Laboratory at Georiga State University 
  Principle author contact: sfilaliboubrahimi1@cs.gsu.edu

This file is part of SpatioTemporalInterpolation*.

SpatioTemporalInterpolation* is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; only version 2
of the License. See the COPYING file for more information.
////////////////////////////////////////////////////////////////////*/

package interpolation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.math3.util.Pair;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Polygon;

import net.sf.javaml.distance.fastdtw.dtw.TimeWarpInfo;
import net.sf.javaml.distance.fastdtw.timeseries.TimeSeries;
import solgrind.base.EventType;
import solgrind.base.Instance;
import solgrind.base.types.essential.TGPair;
import solgrind.base.types.essential.Trajectory;
import util.Cluster;
import util.InterpolationConstants;
import util.KMeans;
import util.PosisitonEstimator;

public class FilamentPolygonInterpolator extends ComplexPolygonInterpolator{
	@Override
	public Instance interpolate(Instance ins) {

		System.out.println("\t\t\t\t\tInterpolating instance.. " + ins.getId() );
		Trajectory iTraj = new Trajectory();
		if(! isValidEventType(ins.getType()) ){
			System.out.println("Polygon is invalid");
			return null;
		} 
		//interpolate here
		Instance adjusted = Interpolator.adjustTimestamps(ins);
		//System.out.println("\t\tAdjusted::: \n" + adjusted);
		ArrayList<TGPair> tgpList = new ArrayList<TGPair>(adjusted.getTrajectory().getTGPairs());
		for(int i = 0; i < tgpList.size(); i++){

			TGPair tgp = tgpList.get(i);
	//		System.out.println("Interpolating... " + tgp);
			TGPair nextTgp = null;
			if (i != tgpList.size()-1){
				nextTgp = tgpList.get(i+1);
			} 
			System.out.println("Interpolating... InstanceID: " + ins.getId() + " Time: " + tgp.getTInterval());
			TreeSet<TGPair> interpolated = interpolateTGPair(tgp, nextTgp,ins.getType());
			cleanEmptyPolygons(interpolated);
			iTraj.getTGPairs().addAll(interpolated);
		}
		iTraj.addTGPair(tgpList.get(tgpList.size()-1));
		Instance iIns = new Instance(ins.getId(), ins.getType(), iTraj);
	//			System.out.println("\t\tInterpolated::: \n" + iIns);
		return iIns;
	}

	@Override
	public boolean isValidEventType(EventType e) {
		return e.getType().equalsIgnoreCase("FI");
	}

	@Override
	public TreeSet<TGPair> interpolateTGPair(TGPair tgp, TGPair nextTgp, EventType et) {
		boolean arealValid = false;
		TreeSet<TGPair> interpolated = new TreeSet<TGPair>();
		TGPair tgp_i_start = new TGPair(tgp.getTInterval().getStartTime(),
				tgp.getTInterval().getStartTime() + InterpolationConstants.I_INTERVAL,
				tgp.getGeometry(),tgp.getKBarchivID(),false);
				interpolated.add(tgp_i_start);
		System.out.println("KB Arhiv id is "+ tgp.getKBarchivID() );


		if(nextTgp == null){
		/*	nextTgp = new TGPair(tgp.getTInterval().getEndTime(),
					tgp.getTInterval().getEndTime() + 1, 
					tgp.getGeometry());*/
			PosisitonEstimator pe= new PosisitonEstimator();
			long ts = tgp.getTInterval().getStartTime();
			long te = tgp.getTInterval().getEndTime()+ InterpolationConstants.getEventPropagation(et);
			for(long i = ts + InterpolationConstants.I_INTERVAL; i < te; i += InterpolationConstants.I_INTERVAL){
				TGPair tgpi = createInterpolatedTGPair(i, i+InterpolationConstants.I_INTERVAL, pe.getPredictedPos((Polygon)tgp.getGeometry(),(i - ts)).getCoordinates());
				interpolated.add(tgpi);
			} //end of interpolation	
			return interpolated;

		}
		Polygon densifiedGeometry = (Polygon) validateGeometry(Interpolator.densify((Polygon) tgp.getGeometry()));	
		Coordinate endpointsDensifiedGeom= findBestEndpoint(densifiedGeometry);
		Polygon reorganizedDensifiedPolygon=(Polygon) FilamentPolygonInterpolator.rearrangePolygon(densifiedGeometry, endpointsDensifiedGeom);
		TimeSeries tgpTS= convertToTimeseries(reorganizedDensifiedPolygon);
		
		
		Polygon densifiedNextGeom = (Polygon)validateGeometry(Interpolator.densify((Polygon) nextTgp.getGeometry()));
		Coordinate endpointsDensifiedNextGeom= findBestEndpoint(densifiedNextGeom);
		Polygon reorganizedDensifiedNextPolygon=(Polygon) FilamentPolygonInterpolator.rearrangePolygon(densifiedNextGeom, endpointsDensifiedNextGeom);
		TimeSeries nexttgpTS= convertToTimeseries(reorganizedDensifiedNextPolygon);
		
	//	interpolated.add(tgp_i_start);//add original to the beginning
		//start interpolating
		long ts = tgp.getTInterval().getStartTime();
		long te = nextTgp.getTInterval().getStartTime();
		TimeWarpInfo warpInfo = getWarpInfo(tgpTS,nexttgpTS);
		for(long i = ts + InterpolationConstants.I_INTERVAL; i < te; i += InterpolationConstants.I_INTERVAL){
			double factor = (double)(i - ts) / (double)(te - ts);
			Coordinate[] i_coordinates = createInterpolatedCoordinates(densifiedGeometry,
									densifiedNextGeom, warpInfo, factor);
			TGPair tgpi = createInterpolatedTGPair(i, i+InterpolationConstants.I_INTERVAL, i_coordinates);
			interpolated.add(tgpi);
		} //end of interpolation
		arealValid = arealValidation(interpolated, tgp, nextTgp);
		if(!arealValid){
			ComplexPolygonInterpolator.arealInvalid_count++;
			ArealPolygonInterpolator api=new ArealPolygonInterpolator();
			interpolated=api.interpolateTGPair(tgp, nextTgp, et);
		}
		return interpolated;
	}
	
    /**
     * Uses the 5-step filament interpolation to interpolated a segment trajectory 
     * of 2 tg-pairs.
     *
     * @param tgp The first tg-pair to be used in the interpolation
     * @param nextTgp The second tg-pair to be used in the interpolation
     * @param time The time to be interpolated in 
     * */
	
	@Override
	public TGPair interpolateAtTime(TGPair tgp, TGPair nextTgp, long time) {
		boolean arealValid = false;
		TreeSet<TGPair> interpolated = new TreeSet<TGPair>();

		if(nextTgp == null){
			return null;
		}
		Polygon densifiedGeometry = (Polygon) validateGeometry(Interpolator.densify((Polygon) tgp.getGeometry()));	
		Coordinate endpointsDensifiedGeom= findBestEndpoint(densifiedGeometry);
		Polygon reorganizedDensifiedPolygon=(Polygon) FilamentPolygonInterpolator.rearrangePolygon(densifiedGeometry, endpointsDensifiedGeom);
		TimeSeries tgpTS= convertToTimeseries(reorganizedDensifiedPolygon);
		
		
		Polygon densifiedNextGeom = (Polygon)validateGeometry(Interpolator.densify((Polygon) nextTgp.getGeometry()));
		Coordinate endpointsDensifiedNextGeom= findBestEndpoint(densifiedNextGeom);
		Polygon reorganizedDensifiedNextPolygon=(Polygon) FilamentPolygonInterpolator.rearrangePolygon(densifiedNextGeom, endpointsDensifiedNextGeom);
		TimeSeries nexttgpTS= convertToTimeseries(reorganizedDensifiedNextPolygon);
		
		//start interpolating
		long ts = tgp.getTInterval().getStartTime();
		long te = nextTgp.getTInterval().getStartTime();
		TimeWarpInfo warpInfo = getWarpInfo(tgpTS,nexttgpTS);
			double factor = (double)(time - ts) / (double)(te - ts);
			Coordinate[] i_coordinates = createInterpolatedCoordinates(densifiedGeometry,
									densifiedNextGeom, warpInfo, factor);
			TGPair tgpi = createInterpolatedTGPair(time, time+InterpolationConstants.I_INTERVAL, i_coordinates);
			interpolated.add(tgpi);
		arealValid = arealValidation(interpolated, tgp, nextTgp);
		if(!arealValid){
			ComplexPolygonInterpolator.arealInvalid_count++;
			ArealPolygonInterpolator api=new ArealPolygonInterpolator();
			return api.interpolateAtTime(tgp, nextTgp, time);
		}
		return interpolated.first();
	}
	/**
	 * Returns the best endpoint between the endpoint pairs. A best endpoint is the one that 
	 * has the highest y coordinate (Filaments do not tilt more than 40 degrees).
	 * @param densifiedGeometry
	 * @return
	 */

	private Coordinate findBestEndpoint(Polygon densifiedGeometry) {
		List<Coordinate> sub=getMostDistantCoordinates(InterpolationConstants.PERCENTAGE_COORDINATE, densifiedGeometry);
		Pair<Coordinate, Coordinate> centroids=getKMeansCentroids(sub);

		KMeans km= new KMeans(sub, centroids);
		List<Cluster> clusters=km.run();
		
		Pair<Coordinate, Coordinate> endpoints=findEndpointsfromClusters(clusters);

		if(endpoints.getFirst().y>=endpoints.getSecond().y) return endpoints.getFirst();
		else return endpoints.getSecond();

	}

	/**
	 *  Finds a percentage of the most distant coordinate pairs of a polygon
	 * @param PERCENTAGE_COORDINATE
	 * @param polygon
	 * @return a percentage of most distant coordinate pairs
	 */
	public static List<Coordinate> getMostDistantCoordinates(double PERCENTAGE_COORDINATE, Polygon polygon ){
		TreeMap<Double, Coordinate> CentroiddistanceCoorindateMap=new TreeMap<Double, Coordinate>(Collections.reverseOrder());
		Coordinate centroid=polygon.getCentroid().getCoordinate();
		ArrayList<Coordinate> sorted= new ArrayList<Coordinate>();

		for(int j=0;j<polygon.getNumPoints();j++){
			double distance = polygon.getCoordinates()[j].distance(centroid);
			CentroiddistanceCoorindateMap.put(distance, polygon.getCoordinates()[j]);
		}	
			
		Set<Entry<Double, Coordinate>> mappings = CentroiddistanceCoorindateMap.entrySet();
		for(Entry<Double, Coordinate> mapping : mappings){ 
			//System.out.println(mapping.getKey() + " ==> " + mapping.getValue().toString()); 
			sorted.add(mapping.getValue());
			}
		
		List<Coordinate> sub =sorted.subList(0, (int) (PERCENTAGE_COORDINATE*polygon.getNumPoints()));
		return sub;
	} 
	
	/**
	 *  Returns the centroids of the clusters of the 2-Means algorithm.
	 * @param mostDistantCoordinates
	 * @return
	 */
	public static Pair<Coordinate, Coordinate> getKMeansCentroids(List<Coordinate> mostDistantCoordinates){
		TreeMap<Double, ArrayList<Coordinate>> distanceCoorindateMap=new TreeMap<Double, ArrayList<Coordinate>>(Collections.reverseOrder());
		Pair<Coordinate, Coordinate>  centroids;

		for(int k=0;k<mostDistantCoordinates.size();k++){
				for(int j=k+1;j<mostDistantCoordinates.size();j++){
					ArrayList<Coordinate> pair = new ArrayList<Coordinate>();
					pair.add(mostDistantCoordinates.get(k));
					pair.add(mostDistantCoordinates.get(j));					
					double distance= pair.get(0).distance(pair.get(1));
					distanceCoorindateMap.put(distance, pair);
				}
			}
			
			double maxdistance= distanceCoorindateMap.firstKey();
			centroids= new Pair<Coordinate, Coordinate>(distanceCoorindateMap.get(maxdistance).get(0),
					distanceCoorindateMap.get(maxdistance).get(1));
			return centroids;

	} 
	
	/**
	 *  Finds the most distant pairs of coordinates belonging to 2 different clusters
	 * @param clusters
	 * @return the two endpoints of a polygon
	 */
	public static Pair<Coordinate, Coordinate>  findEndpointsfromClusters(List<Cluster> clusters){
		TreeMap<Double, ArrayList<Coordinate>> distanceBetweenClusterElementsMap=new TreeMap<Double, ArrayList<Coordinate>>(Collections.reverseOrder());
		Pair<Coordinate, Coordinate>  endpoints;
				
		for(int i=0;i<clusters.get(0).Coordinates.size();i++){
			for(int j=0;j<clusters.get(1).Coordinates.size();j++){
				ArrayList<Coordinate> pair = new ArrayList<Coordinate>();
				pair.add(clusters.get(0).Coordinates.get(i));
				pair.add(clusters.get(1).Coordinates.get(j));				
				double distance= pair.get(0).distance(pair.get(1));

				distanceBetweenClusterElementsMap.put(distance, pair);
			}
		}
		double maxdistance= distanceBetweenClusterElementsMap.firstKey();
		endpoints = new Pair<Coordinate, Coordinate>(distanceBetweenClusterElementsMap.get(maxdistance).get(0),
				distanceBetweenClusterElementsMap.get(maxdistance).get(1));
		return endpoints;
	}
	
	/**
	 * Reorganize a polygon such that it starts(and ends) with the input Coordinate. 	
	 * @param polygon
	 * @param endpoint
	 * @return the new re-arragend geometry that starts from the best endpoint
	 */
	 static Geometry rearrangePolygon(Polygon polygon, Coordinate endpoint) {
		int index=Integer.MAX_VALUE;
		
		for(int i=0;i<polygon.getNumPoints();i++){
			if(polygon.getCoordinates()[i].x== endpoint.x && polygon.getCoordinates()[i].y== endpoint.y){
				index=i;
			//	System.out.println("Found the endpoint in  "+i);
				break;
				}
			}
		if(index==Integer.MAX_VALUE) {
			System.out.println("Impossible, the endpoint is not in the polygon!!");
			return null;
			}
		else return shiftVerticesByOffset(polygon,index);
	}
}