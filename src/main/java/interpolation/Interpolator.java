/*////////////////////////////////////////////////////////////////////////
Interpolator.java

Copyright (C) 2017  Soukaina Filali Boubrahimi, Berkay Aydin, Dustin Kempton, Rafal Angryk
  Data Mining Laboratory at Georiga State University 
  Principle author contact: sfilaliboubrahimi1@cs.gsu.edu

This file is part of SpatioTemporalInterpolation*.

SpatioTemporalInterpolation* is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; only version 2
of the License. See the COPYING file for more information.
////////////////////////////////////////////////////////////////////*/
package interpolation;

import java.util.ArrayList;
import java.util.TreeSet;

import com.vividsolutions.jts.densify.Densifier;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Polygon;

import solgrind.base.EventType;
import solgrind.base.Instance;
import solgrind.base.types.essential.TGPair;
import solgrind.base.types.essential.TInterval;
import solgrind.base.types.essential.Trajectory;
import util.InterpolationConstants;
import util.InterpolationConstants.RoundingStrategy;

public abstract class Interpolator {
	
	public abstract Instance interpolate(Instance ins);
	public abstract boolean isValidEventType(EventType e);
	public abstract TreeSet<TGPair> interpolateTGPair(TGPair tgp, TGPair nextTgp, EventType et);
	public abstract TGPair interpolateAtTime(TGPair tgPair, TGPair tgPair2, long i);
	public static Instance adjustTimestamps(Instance ins){
		
		Trajectory aligned = alignTimestamps( ins.getTrajectory(), ins.getType() );
		Instance adjusted = new Instance(ins.getId(), 
				new EventType(ins.getType().getType()), aligned);
		
		return adjusted;
		
	}

	/**
	 * This aligns the timestamps of the trajectory
	 * First, it does the rounding (or truncating)
	 * Then, it moves the end times to the starting times of the next
	 * Then, it handles the last TGPair (if there is only one TGPair, 
	 * then it also adjusts the last TGPair
	 * @param trajectory - trajectory object to be aligned
	 * @param eventType - event type necessary for the last one
	 * @return - trajectory whose timestamps are aligned
	 */
	private static Trajectory alignTimestamps(Trajectory trajectory, EventType eventType) {
		Trajectory newTraj = new Trajectory();
		ArrayList<TGPair> tgpList = new ArrayList<TGPair>(trajectory.getTGPairs());
		
		for(int i = 0; i < tgpList.size()-1; i++){

			TGPair prevTgp = tgpList.get(i);
			TGPair nextTgp = tgpList.get(i+1);
			
			long prevStartTime = prevTgp.getTInterval().getStartTime();
			long nextStartTime = nextTgp.getTInterval().getStartTime();
			prevStartTime = alignTimestamp(prevStartTime);
			nextStartTime = alignTimestamp(nextStartTime);
			
			long prevEndTime = nextStartTime;
			Geometry geom = prevTgp.getGeometry();
			String kbArchivID= prevTgp.getKBarchivID();
			TGPair newTgp = new TGPair(prevStartTime, prevEndTime, geom, kbArchivID);
			newTraj.addTGPair(newTgp);
		}
		
		//for the last one
		TGPair lastTgp = tgpList.get(tgpList.size()-1);
		long startTime = alignTimestamp(lastTgp.getTInterval().getStartTime());
		long tempEnd = alignTimestamp(lastTgp.getTInterval().getEndTime());

		
		
		long endTime = 0;
		if(tgpList.size() == 1){
			if(tempEnd - startTime > InterpolationConstants.getEventPropagation(eventType)){
				endTime = tempEnd;
			} else{
				endTime = startTime + InterpolationConstants.getEventPropagation(eventType);
			}
		} else if(tgpList.size() > 1){
			if(tempEnd - startTime > InterpolationConstants.I_INTERVAL){
				
				endTime = tempEnd;
			} else{
				endTime = startTime + InterpolationConstants.I_INTERVAL;
			}
		} else{
			System.out.println("Error: There are no tgpairs to interpolate: alignTimestamps()");
		}
		newTraj.addTGPair(new TGPair(startTime, endTime, lastTgp.getGeometry(),lastTgp.getKBarchivID(),false));
		
		return newTraj;
	}

	/**
	 * Aligns the timestamp (say it is 23:14:32 -> it makes it 23:10:00)
	 * @param timestamp - the one to be aligned
	 * @return - aligned timestamp
	 */
	private static long alignTimestamp(long timestamp) {
		if(InterpolationConstants.ROUNDING == RoundingStrategy.DOWN){
			timestamp = (timestamp - InterpolationConstants.EPOCH);
			long remainder = timestamp % InterpolationConstants.I_INTERVAL;
			timestamp -= remainder;
		} else if(InterpolationConstants.ROUNDING == RoundingStrategy.UP){
			timestamp = (timestamp - InterpolationConstants.EPOCH);
			long remainder = timestamp % InterpolationConstants.I_INTERVAL;
			if(remainder != 0){
				timestamp += ( InterpolationConstants.I_INTERVAL - remainder);
			}
		} else if(InterpolationConstants.ROUNDING == RoundingStrategy.ROUND){
			timestamp = (timestamp - InterpolationConstants.EPOCH);
			long remainder = timestamp % InterpolationConstants.I_INTERVAL;
			if(remainder >= InterpolationConstants.I_INTERVAL/2){ //round up
				timestamp += ( InterpolationConstants.I_INTERVAL - remainder);
			} else{ //round down
				timestamp -= remainder;
			}
			
		}
		return timestamp;
	}
	
	public static Geometry densify(Polygon geom){
		double length = geom.getLength();
		double unitLength = length / InterpolationConstants.DENSIFIER_POINT_BOUND; 
		Densifier densifier = new Densifier(geom);
		if(unitLength > 0){
			densifier.setDistanceTolerance(unitLength);
		} else{
			//Maybe we should not densify
			System.out.println(geom);
			//return geom;
			densifier.setDistanceTolerance(InterpolationConstants.SIMPLIFIER_DISTANCE_TOLERANCE);
		}
		return densifier.getResultGeometry();
	}
	public static Interpolator selectInterpolator(EventType e) {
		if(e.getType().equalsIgnoreCase("AR")  || e.getType().equalsIgnoreCase("SS")
				|| e.getType().equalsIgnoreCase("CH")){
			return new ComplexPolygonInterpolator();
		} else if(e.getType().equalsIgnoreCase("EF") || e.getType().equalsIgnoreCase("SG") 
				|| e.getType().equalsIgnoreCase("FL")){
			return new MBRInterpolator();
		}else if(e.getType().equalsIgnoreCase("FI"))
			return new FilamentPolygonInterpolator();
		/*else if(e.getType().equalsIgnoreCase("CH"))
			return new ArealPolygonInterpolator();*/
		
		return null;
	}
}
