package util;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;

import util.CoordinateSystemConverter;
import util.Point2D;

public class PosisitonEstimator {
	static final double THETA = -1.5;

	public Point2D getPredictedPos(Point2D point, double millisecs) {
		Point2D XY = CoordinateSystemConverter.convertHPCToPixXY(point);
		Point2D HGSCoord= CoordinateSystemConverter.convertPixXYToHGS(XY);
		
		HGSCoord = this.calcNewLoc(HGSCoord, millisecs);
		Point2D XY2= CoordinateSystemConverter.convertHGSToPixXY(HGSCoord);
		
		return CoordinateSystemConverter.convertPixXYToHPC(XY2);
	}

	public Polygon getPredictedPos(Polygon poly, double millisecs) {
		Coordinate[] s_coords= new Coordinate[poly.getCoordinates().length];
			
		for (int i = 0; i < poly.getCoordinates().length; i++) {
			Point2D origPoint = new Point2D(poly.getCoordinates()[i].x, poly.getCoordinates()[i].y);
			Point2D shiftedPoint = this.getPredictedPos(origPoint, millisecs);
			
			s_coords[i]= new Coordinate(shiftedPoint.x,shiftedPoint.y);
		}

		Polygon outPoly =new GeometryFactory().createPolygon(s_coords);
		return outPoly;
	}
	Point2D calcNewLoc(Point2D pointIn, double millisecs) {
		double x = pointIn.x + millisecs * (14.44 - 3.0 * Math.pow(Math.sin(Math.toDegrees(pointIn.y)), 2.0))/(24*60*60000);
		pointIn.x = x;

		return pointIn;
	}
}
