package solgrind;

public class SolgrindConstants {
	
	public static final String DATASET_DIR = "/Users/ahmetkucuk/Documents/Research/BERKAY/1Mo_out/";


//	public static final String[] EVENT_TYPES = new String[]{"ar", "ch", "fi", "ss", "ef", "sg"};

	/*
	 * Event Co-occurrence parameters
	 */
	//cce threshold
	public static final double CCE_th = 0.01;
	//pi threshold
//	public static final double PI_th = 0.01;

	public static final long SAMPLING_INTERVAL = 1;

	/*
	 * Event EventEventSequence parameters
	 */
	//head interval (in seconds)
	public static final long H_in = 24; //3600 seconds is an hour
	public static final double H_R = 0.2; //3600 seconds is an hour
	//tail interval (in seconds)
	public static final long T_in = 24;
	public static final double T_R = 0.5;
	//head interval (in seconds)
	public static final long TV = 24;
	//buffer distance (in arcsec)
	public static final double BD = 25;
	//ci threshold
	public static final double CI_th = 0.01;

}
